﻿Shader "Custom/MultParalax"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _NormalMap ("Normal map", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
        _Offset ("Offset", Range(-1,1)) = 0.0
        _nPow ("Normal pow", Range(0,5)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        #pragma surface surf Standard fullforwardshadows

        #pragma target 3.0
        sampler2D _MainTex;
        sampler2D _NormalMap;
        struct Input
        {
            float2 uv_MainTex;
        };

        half    _Glossiness;
        half    _Metallic;
        fixed4  _Color;
        half    _Offset;
        half    _nPow;

        UNITY_INSTANCING_BUFFER_START(Props)
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            _Offset /= 100;

            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;

            fixed2 dUV = mul(UNITY_MATRIX_IT_MV, IN.uv_MainTex - .5);
            fixed2 pUV = fixed2(IN.uv_MainTex.x + dUV.x, IN.uv_MainTex.y + dUV.y * _Offset);

            fixed4 oc = tex2D (_MainTex, pUV + _Offset) * _Color;
            //oc += tex2D (_MainTex, pUV + _Offset * 2) * _Color;
            //oc += tex2D (_MainTex, pUV + _Offset * 3) * _Color;
            //oc += tex2D (_MainTex, pUV + _Offset * 4) * _Color;
            //oc += tex2D (_MainTex, pUV + _Offset * 5) * _Color;
            //oc += tex2D (_MainTex, pUV + _Offset * 6) * _Color;
            //oc += tex2D (_MainTex, pUV + _Offset * 7) * _Color;
            
            //oc /= 8;
            c += oc/2;

            o.Albedo = c;

            fixed4 n = tex2D (_NormalMap, IN.uv_MainTex);
            //o.Normal = UnpackNormal(n * _nPow);
            
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
