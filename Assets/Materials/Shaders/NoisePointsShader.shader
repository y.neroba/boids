Shader "Custom/NoisePointsShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _NoiseTex ("Noise", 2D) = "white" {}
        _ColorA ("Color A", Color) = (1,1,1,1)
        _ColorB ("Color B", Color) = (1,1,1,1)

                [Space]
        _Size ("Size", Float) = 1
        
        _SpeedA ("Speed", Float) = 1
        _SpeedB ("Speed", Float) = 1
        _SizeA ("Size", Float) = 1
        _SizeB ("Size", Float) = 1
                [Space]
        _Pow ("Pow", Float) = 1
        _Value ("Value", Float) = 1
        _Offset ("Offset", Float) = 1
        _Effect ("Effect", Range(0,1)) = 1
    }
    SubShader
    {
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            sampler2D _NoiseTex;
            float _SpeedA;
            float _SpeedB;
            float _SizeA;
            float _SizeB;
            float _Size;
            float _Pow;
            float _Value;
            float _Offset;
            float _Effect;
            fixed4 _ColorA;
            fixed4 _ColorB;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed2 uv = i.uv;
                fixed2 i_uv = ceil(uv * _Size);
                // uv = i_uv;
                fixed4 main = tex2D(_MainTex, uv);
                fixed text = tex2D(_MainTex, uv + fixed2(_Offset, _Offset)).b;
                text = abs(text - tex2D(_MainTex, uv + fixed2(_Offset, -_Offset)).b) < .01;
                text = abs(text - tex2D(_MainTex, uv + fixed2(_Offset, _Offset)).b) < .01;
                text = abs(text - tex2D(_MainTex, uv + fixed2(-_Offset, -_Offset)).b) < .01;
                text = abs(text - tex2D(_MainTex, uv + fixed2(-_Offset, _Offset)).b) < .01;
                fixed speedA = _SpeedA * _Time.x;
                fixed speedB = _SpeedB * _Time.x;
                fixed4 noiseA = tex2D(_NoiseTex, uv * _SizeA + speedA);
                fixed4 noiseB = tex2D(_NoiseTex, fixed2(uv.x + speedB, uv.y) * _SizeB);

                fixed c = pow(noiseA + noiseB, _Pow);
                fixed4 col = abs(sin(27.0 * c - _Time.x * 2));
                fixed4 res = lerp(_ColorA, _ColorB, col.r);
                res += abs((sin(i.uv.x) + .5) * .5);
                fixed avg = text * .3333;
                fixed solver = avg < _Value;
                res = lerp(res, text , solver);
                res = lerp(main, res, _Effect);
                return res;
            }
            ENDCG
        }
    }
}
