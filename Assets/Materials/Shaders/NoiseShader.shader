Shader "Custom/NoiseShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Size ("Size", Range(0,100)) = 1
        _AnimSpeed ("Anim speed", Range(0,100)) = 1
        _ColorTop ("Top Color", Color) = (1,1,1,1)
        _ColorBot ("Bot Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        //Blend SrcAlpha OneMinusSrcAlpha
        Cull Off ZWrite Off ZTest Always
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            fixed _AnimSpeed;
            fixed _Size;
            sampler2D _MainTex;
            fixed4 _MainTex_ST;
            fixed4 _ColorTop;
            fixed4 _ColorBot;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed2 random(fixed2 p) 
            {
                return frac(sin(fixed2(dot(p,fixed2(127.1,311.7)),dot(p,fixed2(269.5,183.3))))*43758.5453);
            }


            float noise (in fixed2 _st) {
                fixed2 i = floor(_st);
                fixed2 f = frac(_st);

                // Four corners in 2D of a tile
                float a = random(i);
                float b = random(i + fixed2(1.0, 0.0));
                float c = random(i + fixed2(0.0, 1.0));
                float d = random(i + fixed2(1.0, 1.0));

                fixed2 u = f * f * (3.0 - 2.0 * f);

                return (a, b, u.x) +
                        (c - a)* u.y * (1.0 - u.x) +
                        (d - b) * u.x * u.y;
            }
            
            fixed random2(fixed x, fixed t) 
            {
                fixed amplitude = 1.;
                fixed frequency = 1.;
                fixed y = sin(x * frequency);
                // float t = 0.01*(-u_time*130.0);
                y += sin(x*frequency*2.1 + t)*4.5;
                y += sin(x*frequency*1.72 + t*1.121)*4.0;
                y += sin(x*frequency*2.221 + t*0.437)*5.0;
                y += sin(x*frequency*3.1122+ t*4.269)*2.5;
                y *= amplitude*0.06;
                return y;
            }

            fixed2 vor(fixed2 uv, fixed2 f_uv, fixed2 offset){
                fixed2 p = random(uv + offset);
                fixed2 pAnim = .5 + .5 * sin(_Time.z * _AnimSpeed + UNITY_TWO_PI * p * 2);
                fixed2 diff = pAnim - f_uv + offset;
                return length(diff);
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                fixed2 uv = i.uv.xy * _Size;
                fixed2 i_uv = floor(uv);
                fixed2 f_uv = frac(uv);
                fixed2 a_uv = i_uv + _Time.z * _AnimSpeed;
                fixed p = random2(a_uv.x, a_uv.y);
                
                // col -= step(.7,abs(sin(27.0 * dist - _Time.z * 2)));
                fixed4 col = lerp(_ColorBot,_ColorTop, 1 - p);
                return col;
            }
            ENDCG
        }
    }
}
