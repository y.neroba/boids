Shader "Custom/FractalShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _XOffset ("X offset", Range(-5,5)) = 1
        _YOffset ("Y offset", Range(0,10)) = 1
        _SizeM ("SizeM", Range(0,1)) = 0
        _SizeD ("SizeD", Range(0,1)) = 0
        _Size ("Size", Range(0,10)) = 1
        _Steps ("Steps", Range(1,500)) = 1
        _CR ("C Real", Range(-3,3)) = 0
        _CI ("C Img", Range(-3,3)) = 0
        _Bound ("Bound", Range(0,2)) = 0
        [Toggle]_Mandel ("Mandel", int) = 0
        _Color1 ("Color 1", Color) = (1,1,1,1)
        _Color2 ("Color 2", Color) = (1,1,1,1)
    }
    SubShader
    {
        Cull Off ZWrite Off ZTest Always
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            int _Steps;
            fixed _CR;
            fixed _CI;
            fixed _Size;
            fixed _SizeD;
            fixed _SizeM;
            fixed _XOffset;
            fixed _YOffset;
            fixed _Bound;
            fixed4 _Color1;
            fixed4 _Color2;
            int _Mandel;

            fixed2 comPow(fixed2 z)
            {
                z = fixed2(z.x * z.x - z.y * z.y, z.x * z.y + z.y * z.x);
                return z;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed size = _Size + _SizeD * .1 + _SizeM * .01;
                fixed2 cross = fixed2((i.uv.x - .5) + _XOffset, (i.uv.y - .5)+ _YOffset);
                
                fixed2 c = fixed2(_CR, _CI);
                fixed2 prev = cross * size;
                if (_Mandel > 0){
                    prev = fixed2(_CR, _CI);
                    c = cross * size;
                }

                for(int i = 0; i < _Steps; ++i){
                    prev = comPow(prev) + c;
                }
                
                fixed x = (prev.x);
                fixed y = (prev.y);
                x = abs(x);
                y = abs(y);
                x = x < _Bound;
                y = y < _Bound;

                return lerp(_Color1, _Color2, x + y);
            }
            ENDCG
        }
    }
}
