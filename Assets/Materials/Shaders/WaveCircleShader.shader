Shader "Custom/WaveCircleShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Size ("Size", Range(0,120)) = 1
        _AnimSpeed ("Anim speed", Range(0,100)) = 1
        _AnimSpeed2 ("Anim speed", Range(0,100)) = 1
        _ColorTop ("Top Color", Color) = (1,1,1,1)
        _ColorBot ("Bot Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        //Blend SrcAlpha OneMinusSrcAlpha
        Cull Off ZWrite Off ZTest Always
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            fixed _AnimSpeed;
            fixed _AnimSpeed2;
            fixed _Size;
            sampler2D _MainTex;
            fixed4 _MainTex_ST;
            fixed4 _ColorTop;
            fixed4 _ColorBot;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = fixed4(0,0,0,0);
                fixed2 uv = (i.uv.xy - .5) * _Size;

                fixed x = uv.x;
                fixed y = uv.y;
                fixed t = (_Time.z * _AnimSpeed);
                fixed r = sqrt(pow(x,2) + pow(y,2));
                fixed m = max(1,r);
                fixed s = 1 - (.5 + .5 * sin(2 * t * UNITY_PI - (r * .5)));
                fixed fx = (x + ((x * s) / m));
                // fixed fy = (y + ((y * s) / m));
                fixed f = (uv.x / fx);
                f -= step(.5,abs(sin(27.0 * f - _Time.y * _AnimSpeed2)));
                return lerp(_ColorBot, _ColorTop, f);
            }
            ENDCG
        }
    }
}
