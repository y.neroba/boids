Shader "Custom/DisposeNoiseShader"
{
    Properties
    {
        _Color          ("Color", Color)            = (1,1,1,1)
        _MainTex        ("Albedo (RGB)", 2D)        = "white" {}
        _DissTex        ("Dissolve Text", 2D)       = "white" {}
        _Glossiness     ("Smoothness", Range(0,1))  = 0.5
        _Metallic       ("Metallic", Range(0,1))    = 0.0
        _DisolveMul     ("Mul", Float)              = 0.0
        _Edge           ("Edge size", Float)        = 0.0
        _Cuttoff        ("Cuttoff", Range(-1,0))    = 0.0
        _DissolveRange  ("DissolveRange", Range(0,10))    = 0.0
        _EmissionColor  ("Emission Color", Color)   = (1,1,1,1)
        _EmissionPower  ("Emiss", Float)            = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "Queue"="Transparent"}
        Blend One OneMinusSrcAlpha
        LOD 200
        Cull Off
        ZWrite On

        CGPROGRAM
        #pragma surface surf Standard fullforwardShadows
        #pragma target 3.0

        sampler2D _MainTex;
        sampler2D _DissTex;
        struct Input {
            float2 uv_MainTex;
            float3 worldPos;
        };

        half    _Glossiness;
        half    _Metallic;
        half    _DisolveMul;
        half    _Cuttoff;
        half    _Edge;
        half    _EmissionPower;
        half    _DissolveRange;
        fixed4  _Color;
        fixed4  _EmissionColor;

        UNITY_INSTANCING_BUFFER_START(Props)
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            fixed4  c       = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            fixed4  dc      = tex2D (_DissTex, IN.uv_MainTex * _DisolveMul);
            
            half    cut     = dc.r + _Cuttoff + _Edge;

            half    center  = ((IN.worldPos < _DissolveRange) | (IN.worldPos < -_DissolveRange)) - .01;
            
            half    notEmiss = dc.r + _Cuttoff;
            
            clip(center);
            
            o.Albedo        = c.rgb;
            o.Metallic      = _Metallic;
            o.Smoothness    = _Glossiness;

            half    addC    = notEmiss < 0;
            //o.Emission      = addC * _EmissionPower * _EmissionColor;
            
            o.Alpha         = 1;
        }
        ENDCG
    }
}
