Shader "Hidden/BlurDifferenceShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _CrossTex ("Cross Texture", 2D) = "white" {}
        _Steps ("Steps", Range(1, 20)) = 1
        _Offset ("Offset", Range(0, .01)) = .1
        _OffsetMul ("OffsetMul", Range(0, 10)) = .1
        _Tau ("Tau", Range(0, 10)) = .1
        _CrossSize ("Cross size", Range(0, 10)) = .1
        _Activation ("Activation", Range(0,1)) = 1

        _Color1 ("Test color 1", Color) = (1,1,1,1)
        _Color2 ("Test color 2", Color) = (1,1,1,1)
    }
    SubShader
    {
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            sampler2D _CrossTex;
            fixed _Offset;
            fixed _Steps;
            fixed _Tau;
            fixed _OffsetMul;
            fixed _CrossSize;
            fixed _Activation;
            
            fixed4 _Color1;
            fixed4 _Color2;

            fixed4 GetSquad(fixed2 uv, fixed x1,fixed x2,fixed y1,fixed y2, fixed offset){
                fixed4 ag = 0;
                for(int i = x1; i < x2; i++){
                    for(int j = y1; j < y2; j++){
                        ag += tex2D(_MainTex, fixed2(uv.x + offset * i, uv.y + offset * j));
                    }
                }
                ag /= ((_Steps * _Steps) * 4);
                return ag;
            }

            fixed GetDif(fixed2 uv, fixed4 ag, fixed offset){
                fixed4 ag2 = GetSquad(uv, -_Steps, _Steps, -_Steps, _Steps, offset);
                fixed4 dif1 = ((1 + _Tau) * ag) - ag2 * _Tau;
                fixed4 cDif1 = (pow(dif1, 2));
                return cDif1;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed2 uv = i.uv;
                fixed4 col = tex2D(_MainTex, uv);

                fixed4 col1 = tex2D(_CrossTex, uv * _CrossSize);
                fixed4 col2 = tex2D(_CrossTex, fixed2(uv.x, uv.y * -1) * _CrossSize);
                fixed4 col3 = tex2D(_CrossTex, fixed2(uv.x * -2, uv.y) * _CrossSize);
                fixed4 col4 = tex2D(_CrossTex, fixed2(uv.x * -2, uv.y * -1) * _CrossSize);
                
                _Steps = floor(_Steps);
                
                fixed4 ag1 = GetSquad(uv, -_Steps, _Steps, -_Steps, _Steps, _Offset);
                fixed4 cDif1 = GetDif(uv, ag1, _Offset * _OffsetMul);
                // fixed4 cDif2 = GetDif(uv, ag1, _Offset * _OffsetMul * 10);
                // fixed4 cDif3 = GetDif(uv, ag1, _Offset * _OffsetMul * 20);
                // fixed4 cDif4 = GetDif(uv, ag1, _Offset * _OffsetMul * 30);

                fixed4 res = col1 + cDif1;
                res = ceil(cDif1 * 4) / 4;
                // res = 1 - res;
                fixed c1 = res.r > .2;
                fixed c2 = res.r > .4;
                fixed c3 = res.r > .6;
                fixed c4 = res.r > .8;

                res = c1 + col1;
                res *= c2 + col2;
                res *= c3 + col3;
                res *= c4 + col4;
                // res = c1 * col1;
                // res *= col2 + cDif2;
                // res *= col3 + cDif3;
                // res *= col4 + cDif4;

                // res -= step(.5,abs(sin(27.0 * res.r - _Time.z * 2)));
                res = lerp(col, fixed4(1,1,1,1), res.r);
                return res;
            }
            ENDCG
        }
    }
}
