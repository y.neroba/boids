Shader "Custom/SinesShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Size ("Size", Range(0,300)) = 1
    }
    SubShader
    {
        Cull Off ZWrite Off ZTest Always
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }
            
            fixed2 random(fixed2 p) 
            {
                return frac(sin(fixed2(dot(p,fixed2(127.1,311.7)),dot(p,fixed2(269.5,183.3))))*43758.5453);
            }

            fixed random2(fixed x, fixed amplitude, fixed frequency)
            {
                float u_time = _Time.z;
                float y = sin(x * frequency);
                float t = 0.01*( -u_time * 130.0);
                y += sin(x * frequency * 2.1 + t ) * 4.5;
                y += sin(x * frequency * 1.72 + t * 1.121) * 4.0;
                y += sin(x * frequency * 2.221 + t * 0.437) * 5.0;
                y += sin(x * frequency * 3.1122+ t * 4.269) * 2.5;
                y *= amplitude * 0.06;
                return y;
            }

            float _Size;

            float noise (fixed2 st) {
                fixed2 i = floor(st);
                fixed2 f = floor(st);
            
                // Four corners in 2D of a tile
                float a = random(i);
                float b = random(i + fixed2(1.0, 0.0));
                float c = random(i + fixed2(0.0, 1.0));
                float d = random(i + fixed2(1.0, 1.0));
            
                fixed2 u = f * f * (3.0 - 2.0 * f);
            
                return lerp(a, b, u.x) + (c - a)* u.y * (1.0 - u.x) + (d - b) * u.x * u.y;
            }

            float fbm (fixed2 st) {
                // Initial values
                float value = 0.0;
                float amplitude = .5;
                float frequency = 0.;
                //
                // Loop of octaves
                for (int i = 0; i < _Size; i++) {
                    value += amplitude * noise(st);
                    st *= 2.;
                    amplitude *= .5;
                }
                return value;
            }

            sampler2D _MainTex;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed2 uv = i.uv;
                fixed2 i_uv = floor(uv * _Size);
                fixed4 col = tex2D(_MainTex, i.uv);
                //col.r = sin(uv.x * 10 * UNITY_PI);
                float amplitude = 1;
                float frequency = 1;
                float x = uv.x * _Size;
                float y = uv.y * _Size;
                //col.r = random2(x, amplitude, frequency * 2);
                //col.g = random2(y, amplitude, frequency);
                col.r = random2(y + x, amplitude, frequency);
                col.r = fbm(uv);
                return col;
            }
            ENDCG
        }
    }
}
