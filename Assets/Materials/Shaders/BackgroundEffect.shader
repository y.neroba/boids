﻿Shader "Custom/BackgroundEffect"
{
    Properties
    {
        _MainTex        ("Texture",             2D)                 = "white" {}
        _ColorTop       ("BGColorTop",          Color)              = (1,1,1,1)
        _ColorBottom    ("BGColorBottom",       Color)              = (1,1,1,1)
        _FlowSpeed      ("FlowSpeed",           Range(-10,10))      = 1    
        _Size           ("ImageSize",           Range(0.001, 0.05)) = 0.1
        _ImageAlpha     ("Alpha",               Range(0, 1))        = 1

    }
    SubShader
    {
        Tags{"Queue"="Transparent" "RenderType"="Transparent" }
        Cull Off ZWrite Off

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };
            
            struct v2f
            {
                float2 uv : TEXCOORD0;
                float2 uv_scull : TEXCOORD1;
                float2 uv_far_scull : TEXCOORD2;
                float2 uv_mid_scull : TEXCOORD3;
                float4 vertex : SV_POSITION;
            };

            sampler2D   _MainTex;
            fixed4      _MainTex_TexelSize;
            fixed4      _ColorTop;
            fixed4      _ColorBottom;
            half        _FlowSpeed;
            half        _Size;
            half        _ImageAlpha;
            float4      _CustomTime;
            float4      _ScreenResolution;
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);

                float3x3 rotator = float3x3 (
                    -.5, .5, 0,
                     .5, .5, 0,
                      0,  0, 1
                );

                float2 skull_uv = mul(rotator, v.uv * _ScreenResolution.xy * _Size);
                o.uv            = v.uv;
                o.uv_scull      = skull_uv + float2(_CustomTime.y, _CustomTime.x) * _FlowSpeed;
                o.uv_far_scull  = skull_uv + float2(_CustomTime.y, _CustomTime.x) * _FlowSpeed * .25;
                o.uv_mid_scull  = skull_uv + float2(_CustomTime.y, _CustomTime.x) * _FlowSpeed * .5;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target 
            {
                fixed sY        = sin(i.uv.y * UNITY_PI);
                fixed4 col      = lerp(_ColorBottom, _ColorTop, sY);
                fixed4 imgCol   = tex2D(_MainTex, i.uv_scull);
                fixed4 imgCol2  = tex2D(_MainTex, i.uv_far_scull * 3);
                fixed4 imgCol3  = tex2D(_MainTex, i.uv_mid_scull * 1.5);
                
                col += imgCol * imgCol.a * _ImageAlpha;
                col += imgCol * imgCol2 * max(imgCol.a, imgCol2.a *.25) * _ImageAlpha;
                col += imgCol * imgCol2 * imgCol3 * max(max(imgCol.a, imgCol2.a *.25), imgCol3.a * .5) * _ImageAlpha;
                
                return col;
            }
            ENDCG
        }
    }
}
