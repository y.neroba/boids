﻿Shader "Unlit/SelectedAreaShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color ("Color", Color) = (1,1,1,1)
        _MoveSpeed ("MoveSpeed", Float) = 1
        _Side ("Side size", Range(0,1)) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "Queue"="Transparent" }
        Blend SrcAlpha OneMinusSrcAlpha
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
                float scale : TEXCOORD2;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _Color;
            float _MoveSpeed;
            float _Side;

            v2f vert (appdata v) {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex) + _Time.y * _MoveSpeed;
                o.uv2 = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target {
                fixed4 col = tex2D(_MainTex, i.uv);
                half xxx = min(pow((i.uv2.x * 2) - (_Side * 2), 4) * 2, 1);
                half yyy = min(pow((i.uv2.y * 2) - (_Side * 2), 4) * 2, 1);
                col.a = (xxx + yyy) * .5;
                col.a *= col.r;
                col *= _Color;

                return col;
            }
            ENDCG
        }
    }
}
