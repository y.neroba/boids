Shader "Hidden/FractalIllusion"
{
    Properties
    {
        [HideInInspector] _MainTex ("Texture", 2D) = "white" {}
        _Frequency ("Frequency", Float) = 1
        
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            float _Frequency;

            fixed3 pallete(fixed t)
            {
                fixed3 a = fixed3(.5,.5,.5);
                fixed3 b = fixed3(.5,.5,.5);
                fixed3 c = fixed3(1,1,1);
                fixed3 d = fixed3(0,.15,.2);
                
                return a + b * cos(6.28318 * (c * t * d));
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                fixed2 uv = i.uv * 2 - 1;
                // fixed2 uv = i.uv;
                fixed sL = length(uv);

                fixed3 finalColor = 0;

                for(int iterator = 0; iterator < 3; iterator++)
                {
                    uv *= 1.5;
                    uv = frac(uv);
                    uv -= .5;
                    fixed l = length(uv) * exp(-sL);

                    fixed3 color = pallete(sL + _Time.z);
                    fixed f = _Frequency;// * (sin(_Time.z) * .25 + .67);
                    l = sin(l * f + _Time.z) / f;
                    l = abs(l);
                    l = smoothstep(0, .4, l);
                    l = .01 / l;
                    l = pow(2,l);
                    finalColor += (color * l) / 3;
                }

                fixed4 res = fixed4(finalColor, 1);
                return res;
            }
            ENDCG
        }
    }
}
