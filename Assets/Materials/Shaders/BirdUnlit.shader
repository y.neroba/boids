﻿Shader "Unlit/BirdUnlit"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color ("Col", Color) = (1,1,1,1)
        _Pow ("Power", Float) = 1
        _Speed ("Speed", Float) = 2
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100
		Cull Off

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma multi_compile_instancing

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _Pow;
            float _Speed;

			UNITY_INSTANCING_BUFFER_START(Props)
				UNITY_DEFINE_INSTANCED_PROP(fixed4, _Color)
			UNITY_INSTANCING_BUFFER_END(Props)
            

            v2f vert (appdata v)
            {
                v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				float4 worldPos = mul(unity_ObjectToWorld, v.vertex);
				v.vertex.z += sin(_Time.z * 2 * _Speed) * (v.vertex.x + 1) * sin(abs(v.vertex.x) * 2) * .2 * _Pow;
				v.vertex.x += cos(_Time.z * 4 * _Speed) * (v.vertex.x + 1) * sin(v.vertex.x * 2) * .05 * _Pow;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
            }
			
			fixed4 frag (v2f i) : SV_Target 
			{
                fixed4 col = tex2D(_MainTex, i.uv) * UNITY_ACCESS_INSTANCED_PROP(Props, _Color);
                return col;
            }

			
            ENDCG
        }
    }
}
