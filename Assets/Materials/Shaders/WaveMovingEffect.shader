Shader "Custom/WaveMovingEffect"
{
    Properties
    {
        [HideInInspector]
        _MainTex ("Texture", 2D) = "white" {}
        _Offset ("Offset", Float) = 1
        _ChanelOffset ("ChanelOffset", Range(0,1)) = 1
        _Additive ("Additive", Range(0,1)) = 1
        _Vector ("Vector", Vector) = (0,0,0,0)
    }
    SubShader
    {
        Blend SrcAlpha OneMinusSrcAlpha
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            float _Offset;
            float _ChanelOffset;
            float _Additive;
            float4 _Vector;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
                fixed sinTime = sin(_Time.z) * .25 + .5;
                
                fixed grayScale = dot(col.rgb,float3(0.3,0.59,0.11)) + _Additive;
                
                fixed n = smoothstep(0, 1, grayScale);

                fixed moveKof = _Time.x * _Offset * (n < _ChanelOffset);

                fixed2 moveUV = i.uv * n;

                moveUV = fixed2(moveUV.x - moveKof * _Vector.x, moveUV.y - moveKof * _Vector.y);

                fixed4 col2 = tex2D(_MainTex, moveUV);
                
                fixed4 res = col * col2;
                res.a = col.a;

                return res;
            }
            ENDCG
        }
    }
}
