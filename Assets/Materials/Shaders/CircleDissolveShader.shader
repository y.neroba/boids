Shader "Unlit/CircleDissolveShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Size ("Size", Range(0,10)) = 1
        _Contrast("Contrast", Range(1,5)) = 1
        _Cutoff("Cutoff", Range(0,1)) = 0
        _Power("Power", Range(0,1)) = 0.1
    }
    SubShader
    {
        Blend SrcAlpha OneMinusSrcAlpha
        Cull Off ZWrite Off ZTest Always

        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float2 normal : TEXCOORD1;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            half _Size;
            half _Cutoff;
            half _Contrast;
            half _Power;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                half4 col = tex2D(_MainTex, i.uv);
                half s = sin(i.uv.x * _MainTex_ST.x * _Size);
                half c = cos(i.uv.y * _MainTex_ST.y * _Size);
                half a = sin(i.uv.x + _Time.z) * _Power + 1 + _Power;
                half r = s * c * _Contrast * ((1 - i.uv.y / _MainTex_ST.y) * a);

                col = half4(r,r,r,r - _Cutoff);
                //col = half4(i.uv.x,i.uv.x,i.uv.x,1);
                return col;
            }
            ENDCG
        }
    }
}
