Shader "Custom/VoronoiShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Size ("Size", Range(0,100)) = 1
        _AnimSpeed ("Anim speed", Range(0,100)) = 1
        _ColorTop ("Top Color", Color) = (1,1,1,1)
        _ColorBot ("Bot Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        //Blend SrcAlpha OneMinusSrcAlpha
        Cull Off ZWrite Off ZTest Always
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            fixed _AnimSpeed;
            fixed _Size;
            sampler2D _MainTex;
            fixed4 _MainTex_ST;
            fixed4 _ColorTop;
            fixed4 _ColorBot;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed2 random(fixed2 p) 
            {
                return frac(sin(fixed2(dot(p,fixed2(127.1,311.7)),dot(p,fixed2(269.5,183.3))))*43758.5453);
            }
            fixed2 vor(fixed2 uv, fixed2 f_uv, fixed2 offset){
                fixed2 p = random(uv + offset);
                fixed2 pAnim = .5 + .5 * sin(_Time.z * _AnimSpeed + UNITY_TWO_PI * p * 2);
                fixed2 diff = pAnim - f_uv + offset;
                return length(diff);
            }
            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = fixed4(0,0,0,0);
                fixed2 uv = i.uv.xy * _Size;
                fixed2 i_uv = floor(uv);
                fixed2 f_uv = frac(uv);
                fixed dist = 1;
                fixed2 p = random(i_uv);
                fixed2 pAnimBase = .5 + .5 * sin(_Time.z * _AnimSpeed + UNITY_TWO_PI * p * 2);
                fixed2 pAnim = pAnimBase;
                fixed2 diff = pAnim - f_uv;
                dist = min(dist, length(diff));
                
                dist = min(dist, vor(i_uv, f_uv, fixed2(-1,0)));
                dist = min(dist, vor(i_uv, f_uv, fixed2(1,0)));
                dist = min(dist, vor(i_uv, f_uv, fixed2(-1,-1)));
                dist = min(dist, vor(i_uv, f_uv, fixed2(-1,1)));
                dist = min(dist, vor(i_uv, f_uv, fixed2(0,-1)));
                dist = min(dist, vor(i_uv, f_uv, fixed2(0,1)));
                dist = min(dist, vor(i_uv, f_uv, fixed2(1,-1)));
                dist = min(dist, vor(i_uv, f_uv, fixed2(1,1)));

                col = 1 - dist;
                // col -= step(c, dist);
                col -= smoothstep(.2, .8, abs(sin(27.0 * dist - _Time.z * 2)));
                col = lerp(_ColorBot,_ColorTop,1-col.r);
                return col;
            }
            ENDCG
        }
    }
}
