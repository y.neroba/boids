﻿Shader "Custom/WaterVertexShader"
{
    Properties
    {
        _MainText  ("Texture", 2D) = "white"{}
        _MainColor  ("Color1", Color) = (1,1,1,1)
        _Height  ("Height", Range(0,2)) = 1
        _Speed  ("Speed", Range(0,2)) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        #pragma surface surf Standard fullforwardshadows vertex:vert
        #pragma target 3.0

        struct Input
        {
            float2 uv_MainTex;
            float4 color : COLOR;
        };

        sampler2D _MainTex;
        fixed4 _MainColor;
        fixed _Height;
        fixed _Speed;

        UNITY_INSTANCING_BUFFER_START(Props)
        UNITY_INSTANCING_BUFFER_END(Props)

        void vert(inout appdata_full v) 
        {
            fixed hight = tex2Dlod(_MainTex, v.texcoord2).r;
            v.vertex.y = sin(_Time.z * _Speed + hight) * _Height;
        }

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            o.Albedo = _MainColor;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
