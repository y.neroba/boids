﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/Hologram" {
    Properties {
        _MainTex  ("Texture",   2D) = "white" {}
        _NoiseTex ("Noise",     2D) = "white" {}

        _MainColor  ("Main Color",   Color) = (1,1,1,1)
        _BorderColor  ("Border Color",   Color) = (1,1,1,1)

        _Bias   ("Bias ",   Float) = 1
        _Scale  ("Scale",   Float) = 1
        _Power  ("Power",   Float) = 1
    }
    SubShader {
        Tags { "RenderType"="Opaque" "Queue" = "Transparent" }
        Blend SrcAlpha OneMinusSrcAlpha
        LOD 100
        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f {
                float2 uv : TEXCOORD0;
                float R : TEXCOORD1;
                float wPos : TEXCOORD2;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _NoiseTex;
            float4 _MainTex_ST;
            float4 _MainColor;
            float4 _BorderColor;
            float _Bias ;
            float _Scale;
            float _Power;

            v2f vert (appdata v) {
                v2f o;
                o.vertex    = UnityObjectToClipPos(v.vertex);
                o.uv        = TRANSFORM_TEX(v.uv, _MainTex);

                o.wPos              = mul(unity_ObjectToWorld, v.vertex).xyz;
	            float3 normWorld    = UnityObjectToWorldNormal(v.normal);
	            float3 I            = normalize(o.wPos - _WorldSpaceCameraPos.xyz);
	            o.R                 = _Bias + _Scale * pow(1.0 + dot(I, normWorld), _Power);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target {
                fixed4 lines = tex2D(_NoiseTex, i.vertex.xy * .01) * .1;
                fixed4 col = i.R * _BorderColor;
                col += ((1 - i.R) * _MainColor);
                col *= (1 - lines);
                return col;
            }
            ENDCG
        }
    }
}