Shader "Unlit/BlackWhiteShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color ("Color", Color) = (1,1,1,1)
        _Power ("Power", Range(1,10)) = 2
        _Scale ("Scale", Range(0,10)) = 2
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 normal : NORMAL;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 normal : TEXCOORD1;
                float normalDot : TEXCOORD2;
                float wPos : TEXCOORD3;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _Color;
            float _Power;
            float _Scale;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex            = UnityObjectToClipPos(v.vertex);
                o.wPos              = mul(unity_ObjectToWorld, v.vertex).xyz;
                o.uv                = TRANSFORM_TEX(v.uv, _MainTex);
                o.normal            = v.normal;
	            float3 normWorld    = UnityObjectToWorldNormal(v.normal);
	            
                //o.normalDot = 
                
                float3 I            = normalize(o.wPos - _WorldSpaceLightPos0.xyz);
	            o.normalDot         = pow(1 + dot(I, normWorld), _Power);
                
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
                col = lerp( _Color, col, i.normalDot);
                return col;
            }
            ENDCG
        }
    }
}
