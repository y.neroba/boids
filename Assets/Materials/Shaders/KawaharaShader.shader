Shader "Custom/KawaharaShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Pow ("Pow", Range(0,.1)) = 1
        _Offset ("Offset", Range(0,.1)) = 1
    }
    SubShader
    {
        Blend SrcAlpha OneMinusSrcAlpha
        Cull Off ZWrite Off ZTest Always
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            fixed4 _MainTex_ST;
            fixed _Pow;
            fixed _Offset;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }
            
            fixed4 kawahara(fixed2 uv, fixed4 center){
                fixed4 ct = tex2D (_MainTex, fixed2(uv.x, uv.y + _Offset));
                fixed4 cr = tex2D (_MainTex, fixed2(uv.x + _Offset, uv.y));
                fixed4 ctr = tex2D(_MainTex, fixed2(uv.x + _Offset, uv.y + _Offset));
                fixed4 sum1 = (ct + cr + ctr) * .33333;

                ct = tex2D (_MainTex, fixed2(uv.x, uv.y - _Offset));
                cr = tex2D (_MainTex, fixed2(uv.x - _Offset, uv.y));
                ctr = tex2D(_MainTex, fixed2(uv.x - _Offset, uv.y - _Offset));
                fixed4 sum2 = (ct + cr + ctr) * .33333;
                
                fixed4 res = center;
                fixed dif1 = abs(center - sum1);
                fixed dif2 = abs(center - sum2);
                if ((dif1 - dif2) > _Pow){
                    res = sum1;
                }
                else if ((dif1 - dif2) < -_Pow){
                    res = sum2;
                }
                return res;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 c = tex2D (_MainTex, i.uv);
                fixed4 kaw = kawahara(i.uv.xy, c);
                kaw.a = c.a;
                return kaw;
            }
            ENDCG
        }
    }
}
