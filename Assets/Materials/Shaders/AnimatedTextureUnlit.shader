﻿Shader "Custom/AnimatedTextureUnlit"
{
    Properties
    {
        _MainColor  ("Color1", Color) = (1,1,1,1)
        _MainColor2 ("Color2", Color) = (1,1,1,1)
        _SubColor   ("SubColor", Color) = (1,1,1,1)
        _BlindColor ("Blind Color", Color) = (1,1,1,1)
        _MainTex    ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic   ("Metallic", Range(0,1)) = 0.0
        _EmPower    ("Emission Power", Float) = 1
        _TextAlpha  ("Alpha", Range(0,1)) = 1
        _Scaler     ("Scale", Range(0,5)) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        #pragma surface surf Standard fullforwardshadows /*vertex:vert*/
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
            float4 color : COLOR;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _MainColor  ;
        fixed4 _MainColor2 ;
        fixed4 _BlindColor ;
        fixed4 _SubColor;
        half _EmPower;
        half _TextAlpha;
        half _Scaler;
        UNITY_INSTANCING_BUFFER_START(Props)
        UNITY_INSTANCING_BUFFER_END(Props)

        
        //void vert(inout appdata_full v) {
            
        //}


        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            fixed vertCol = IN.color.r;
            fixed2 nUV = IN.uv_MainTex;
            nUV.y += (_Time.y + _CosTime.z) * .2;
            fixed2 nUV2 = nUV * _Scaler;
            nUV2.y -= _Time.y * .1;
            fixed2 blindUV = IN.uv_MainTex * 10;
            fixed4 resColor = lerp(_MainColor, _MainColor2, _SinTime.z);
            fixed4 text     = tex2D (_MainTex, nUV);
            fixed4 textBack = tex2D (_MainTex, nUV2);
            fixed4 c     = lerp(_SubColor, resColor, vertCol);
            fixed alp    = max(text.a * _TextAlpha, textBack.a * _TextAlpha);
            fixed blick  = sin(blindUV.y - _Time.z * 2);
            
            text.rgb     = 1;
            textBack.rgb = 1;
            textBack.a  *= 0.5;

            fixed3 res   = lerp(c.rgb, max(text.rgb, textBack.rgb), alp * vertCol);
            //o.Albedo     = lerp(res, _BlindColor, blick) * vertCol;
            o.Albedo     = res;
            //o.Emission   = vertCol * max(max(text.a, textBack.a), blick) * _EmPower;
            alp = max(text.a, textBack.a);
            o.Emission   =  vertCol * max(alp, blick) * _EmPower;
            //o.Emission   *= _BlindColor;
            o.Metallic   =  _Metallic;
            o.Smoothness =  _Glossiness * vertCol;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
