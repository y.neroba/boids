Shader "Unlit/EdgesAccent"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Offset ("Offset", Range(0,0.2)) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _Offset;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                half4 col = tex2D(_MainTex, i.uv) * 8;

                half offsetX = _Offset * _MainTex_ST.x;
                half offsetY = _Offset * _MainTex_ST.y;

                float4 r = tex2D(_MainTex, i.uv + half2(offsetX,0)) * -1;
                float4 l = tex2D(_MainTex, i.uv + half2(-offsetX,0)) * -1;
                float4 t = tex2D(_MainTex, i.uv + half2(0,offsetY)) * -1;
                float4 b = tex2D(_MainTex, i.uv + half2(0,-offsetY)) * -1;

                float4 rt = tex2D(_MainTex, i.uv + half2(offsetX,offsetY)) * -1;
                float4 lt = tex2D(_MainTex, i.uv + half2(-offsetX,offsetY)) * -1;
                float4 rb = tex2D(_MainTex, i.uv + half2(offsetX,-offsetY)) * -1;
                float4 lb = tex2D(_MainTex, i.uv + half2(-offsetX,-offsetY)) * -1;
                col += r + l + t + b + lt + rt + rb + lb;

                return col;
            }
            ENDCG
        }
    }
}
