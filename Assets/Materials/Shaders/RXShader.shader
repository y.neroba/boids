Shader "Custom/RXShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Steps ("Steps", Range(1,30)) = 5
        _Offset ("Offset", Range(0,.01)) = 0
        _Activation ("Activation", Range(0,1)) = 1
    }
    SubShader
    {
        //Blend SrcAlpha OneMinusSrcAlpha
        Cull Off ZWrite Off ZTest Always
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            fixed _Activation;
            fixed _Steps;
            fixed _AnimSpeed;
            fixed _AnimSpeed2;
            fixed _Offset;
            fixed _Size;
            sampler2D _MainTex;
            fixed4 _MainTex_ST;
            fixed4 _ColorTop;
            fixed4 _ColorBot;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 GetSquad(fixed2 uv, fixed x1,fixed x2,fixed y1,fixed y2){
                fixed4 ag = 0;
                for(int i = x1; i < x2; i++){
                    for(int j = y1; j < y2; j++){
                        ag += tex2D(_MainTex, fixed2(uv.x + _Offset * i, uv.y + _Offset * j));
                    }
                }
                ag *= (1 / (_Steps * _Steps));
                return ag;
            }

            fixed4 frag (v2f v) : SV_Target
            {
                fixed2 uv = v.uv.xy;
                fixed4 col = tex2D(_MainTex, fixed2(uv));
                _Steps = floor(_Steps);

                fixed4 ag1 = GetSquad(uv, -_Steps, 0, -_Steps, 0);
                fixed4 ag2 = GetSquad(uv, 0, _Steps, 0, _Steps);
                fixed4 ag3 = GetSquad(uv, -_Steps, 0, 0, _Steps);
                fixed4 ag4 = GetSquad(uv, 0, _Steps, -_Steps, 0);

                // fixed d1 = length(col) - length(ag1);
                // fixed d2 = length(col) - length(ag2);
                // fixed d3 = length(col) - length(ag3);
                // fixed d4 = length(col) - length(ag4);
                fixed4 d = fixed4((ag1.rgb + ag2.rgb + ag3.rgb + ag4.rgb) * .25, 1);
                d.r -= step(_Activation,abs(sin(27.0 * d.r - _Time.z * 2)));
                d.g -= step(_Activation,abs(sin(27.0 * d.g - _Time.z * 2)));
                d.b -= step(_Activation,abs(sin(27.0 * d.b - _Time.z * 2)));
                // d.r -= step(_Activation,abs(d.r));
                // d.g -= step(_Activation,abs(d.g));
                // d.b -= step(_Activation,abs(d.b));

                // return d + col;
                return d;
                return dot(d, col) * col + d;
                //f -= step(.5,abs(sin(27.0 * f - _Time.y * _AnimSpeed2)));
                // return col;
            }
            ENDCG
        }
    }
}
