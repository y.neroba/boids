Shader "Unlit/WaterShader"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Texture", 2D) = "white" {}
        _WaveColor ("Wave color", Color) = (1,1,1,1)
        _ColorMult ("Color Mult", Range(0,1)) = 1
        _AnimSpeed ("Animation Speed", Range(0,10)) = 1
        _WaterCutoff ("Water Cutoff", Range(0,1)) = 1
    }
    SubShader
    {
        Blend SrcAlpha OneMinusSrcAlpha
        Cull Off
        Tags { "RenderType" = "Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float4 screenPosition : TEXCOORD1;
            };

            sampler2D _MainTex;
            sampler2D _CameraDepthTexture;
            float4 _MainTex_ST;
            float4 _Color;
            float4 _WaveColor;
            float _AnimSpeed;
            float _ColorMult;
            fixed _WaterCutoff;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.screenPosition = ComputeScreenPos(o.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float depth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, i.screenPosition.xy / i.screenPosition.w));
                fixed normalDepth = 1 - depth * _ColorMult;
                fixed4 colMain = tex2D(_MainTex, i.uv * 1.25 + _Time.y * _AnimSpeed);
                fixed4 colSecond = tex2D(_MainTex, fixed2(i.uv.x - _Time.y * _AnimSpeed, i.uv.y));
                fixed4 col = _Color;
                fixed4 waveColor = lerp(fixed4(_WaveColor.rgb,0), _WaveColor, (colMain.a * colSecond.a) > normalDepth);
                col += waveColor;
                col *= _Color;
                return col;
            }
            ENDCG
        }
    }
}
