Shader "Unlit/ActiveDissolveShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _DissolveTexture ("Texture", 2D) = "white" {}
        _Range ("Range", Range(-0.001,1)) = 0.2
        _DissolveSize ("DissolveSize", Range(0,2)) = 1
        _Size ("Size", Range(0,2)) = 1
        _Offset ("Offset", Range(0,2)) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" }
        Blend SrcAlpha OneMinusSrcAlpha
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv       : TEXCOORD0;
                float4 vertex   : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _DissolveTexture;
            float4 _MainTex_ST;
            float _Range;
            float _Size;
            float _Offset;
            float _DissolveSize;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
                fixed4 dissolve = tex2D(_DissolveTexture, i.uv * _DissolveSize + _Time.x * _DissolveSize);
                dissolve = (dissolve.r + dissolve.g + dissolve.b) * .33333;

                float disVal = 1;
                
                float sinX = -pow(i.uv.x - _Offset * .5, 2) * UNITY_TWO_PI + _Size;
                float sinY = -pow(i.uv.y - _Offset * .5, 2) * UNITY_TWO_PI + _Size;

                disVal *= sinX * sinY;
                disVal >= 0;
                
                float4 fadedColor = col;

                disVal *= sinX > 0;
                float4 fadedDissolve = dissolve * disVal;
                disVal = fadedDissolve > _Range;

                return fadedColor * disVal;
            }
            ENDCG
        }
    }
}
