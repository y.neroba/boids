Shader "Unlit/ToonShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "black" {}
        _EmissionMap ("Emission Texture", 2D) = "black" {}
        _AddColor ("Additional Color", Color) = (1,1,1,1)
        _AddColorSide ("Additional Side Color", Color) = (1,1,1,1)
        _EmissionPower ("Emission Power", Float) = 1
        _EmissionColor ("Emission Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags 
        { 
            "RenderType"="Opaque" 
            "LightMode"="ForwardBase"
        }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase nolightmap nodirlightmap nodynlightmap novertexlight
            #include "AutoLight.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 normal : NORMAL;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                SHADOW_COORDS(1)
                float2 uv : TEXCOORD0;
                float3 diff : COLOR0;
                float3 ambient : COLOR1;
                float4 pos : SV_POSITION;
                float sideDot : TEXCOORD2;
            };

            sampler2D _MainTex;
            sampler2D _EmissionMap;
            float4 _MainTex_ST;
            float4 _EmissionColor;
            float4 _AddColor;
            float4 _AddColorSide;
            float _EmissionPower;
            
            v2f vert (appdata v)
            {
                v2f o;
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.pos = UnityObjectToClipPos(v.vertex);
                half3 worldNormal = UnityObjectToWorldNormal(v.normal);
                o.sideDot = dot(v.normal, fixed3(0,0,1));
                o.diff = dot(worldNormal, _WorldSpaceLightPos0.xyz) * _LightColor0.rgb;
                o.ambient = ShadeSH9(half4(worldNormal,1));
                TRANSFER_SHADOW(o)
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
                col +=  _AddColor * (1 - i.sideDot);
                col +=  _AddColorSide * i.sideDot;
                fixed4 emission = tex2D(_EmissionMap, i.uv) * _EmissionColor * _EmissionPower;
                
                fixed shadow = SHADOW_ATTENUATION(i);
                fixed3 lighting = i.diff * shadow;
                fixed light = .25;
                light += (lighting.r > .25) * .25;
                light += (lighting.r > .5) * .25;
                light += (lighting.r > .75) * .25;
                lighting *= light;
                col += emission;
                col.rgb *= (lighting + i.ambient);
                return col;
            }
            ENDCG
        }

        Pass
        {
            Tags { 
                "RenderType"="Opaque" 
                "LightMode"="ShadowCaster"
            }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 color : COLOR;
            };

            struct v2f
            {
                float4 color : COLOR;
                float4 pos : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.color = v.color;
                o.pos = UnityObjectToClipPos(v.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return i.color;
            }
            ENDCG
        }
    }
}
