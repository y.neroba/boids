﻿Shader "Custom/FluffyTree"
{
    Properties
    {
        _Color      ("Color",           Color)      = (1,1,1,1)
        _MainTex    ("Opacity mask",    2D)         = "white" {}
        _Glossiness ("Smoothness",      Range(0,1)) = 0.5
        _Metallic   ("Metallic",        Range(0,1)) = 0.0
        _Scale      ("Scale",           Range(0,10)) = 1
        _Wind       ("Wind",            Range(0,25)) = 1
        _Effect     ("Effect",          Range(0,1)) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "Queue"="Transparent"}
        Blend SrcAlpha OneMinusSrcAlpha
        LOD 200

        CGPROGRAM
        #pragma surface surf Standard addshadow keepalpha Lambert vertex:vert
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input {
            float2 uv_MainTex;
        };

        half    _Glossiness;
        half    _Metallic;
        fixed4  _Color;
        fixed   _Effect;
        half    _Scale;
        half    _Wind;

        UNITY_INSTANCING_BUFFER_START(Props)
        UNITY_INSTANCING_BUFFER_END(Props)

        void vert (inout appdata_full v, out Input o) {
            UNITY_INITIALIZE_OUTPUT(Input,o);
            float2 uv = v.texcoord.xy;
            float2 remapUV = uv * -2 + 1;
            float3 remapAppUV = float3(remapUV.x,remapUV.y, 0);
            float3 rotUV = mul(remapAppUV, UNITY_MATRIX_IT_MV);
            float3 normUV = normalize(rotUV) * (sin(v.vertex.x * v.vertex.y * _Time.x * _Wind) * .1 + 1);
            float3 resUV = lerp(0, normUV * _Scale, _Effect);
            v.vertex.xyz += resUV;
        }

        void surf (Input IN, inout SurfaceOutputStandard o) {
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
            clip(c.a - _Effect * .01);
            o.Albedo = c.g * _Color * (_Effect > 0);
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Emission = 0;
            o.Alpha = 1;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
