Shader "Custom/ChessTableShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _ColorTop ("Color white", Color) = (1,1,1,1)
        _ColorBot ("Color dark", Color) = (1,1,1,1)
        _Size ("Size", Range(0,30)) = 1
        _AnimSize ("Anim size", Float) = 1
        _AnimSpeed ("Anim speed", Range(0,100)) = 1
    }
    SubShader
    {
        Blend SrcAlpha OneMinusSrcAlpha
        Cull Off ZWrite Off ZTest Always
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            fixed _AnimSpeed;
            fixed _AnimSize;
            fixed _Size;
            sampler2D _MainTex;
            fixed4 _MainTex_ST;
            fixed4 _ColorTop;
            fixed4 _ColorBot;

            v2f vert (appdata v)
            {
                v2f o;
                // v.vertex.y += cos(v.vertex.x + _Time.z * _AnimSpeed) * _AnimSize;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed pi = 3.1415;
                fixed2 nuv = i.uv - .5;
                fixed at = atan2(nuv.x , nuv.y);
                fixed ra = length(nuv);
                fixed2 uv = fixed2(at / pi,  ra);
                fixed size = _Size * pi;
                fixed sizeX = size * _MainTex_ST.x;
                fixed sizeY = size * _MainTex_ST.y;
                fixed s1 = sin(uv.x * sizeX + _AnimSpeed * _Time.z);
                fixed s2 = sin(uv.y * sizeY + _AnimSpeed * _Time.z);
                fixed4 col = (s1 * s2) > 0;
                // fixed a = pow(sin(i.uv.x * pi) * sin(i.uv.y * pi),5);
                col = lerp(_ColorBot, _ColorTop, col.r);
                // col.a = a;
                return col;
            }
            ENDCG
        }
    }
}
