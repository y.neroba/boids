using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Lazy
{
    public class LinesPlacer : MonoBehaviour
    {
        [SerializeField] private GameObject cube;
        [SerializeField] private GameObject cube2;
        [SerializeField] private GameObject[] fence1Exampe;
        [SerializeField] private GameObject[] fence2Exampe;
        [SerializeField] private Transform fenceHolder;
        [SerializeField] private Transform linksHolder;
        [SerializeField] private Transform[] points;
        [SerializeField] private bool snap;
        [SerializeField] private float lineLength = .1f;
        [Header("Prview")]
        [SerializeField] private bool place;

        private List<Vector3> pointsTotalList = new List<Vector3>();

        private void PlaceSubPoints()
        {
            pointsTotalList = new List<Vector3>();
            if (snap)
            {
                for (int i = 0; i < points.Length; i++)
                {
                    var pos = points[i].position;

                    points[i].position = new Vector3(
                        Mathf.Floor(pos.x),
                        Mathf.Floor(pos.y),
                        Mathf.Floor(pos.z)
                        );
                }
            }

            var pointsTotal = points.Length - 1;
            var addLen = 0f;
            for (int i = 0; i < pointsTotal - 1; i++)
            {
                var first = points[i].position;
                var second = points[(i + 1) % pointsTotal].position;
                var dir = second - first;
                var len = dir.magnitude - addLen;
                var pointsInLine = (int)(len / lineLength);
                var negLen = Mathf.Abs(len - (pointsInLine * lineLength));
                var normDir = dir.normalized;
                var prev = first + normDir * addLen;
                dir = normDir * lineLength;

                pointsTotalList.Add(prev);
                for (int j = 0; j < pointsInLine; j++)
                {
                    var p = dir + prev;
                    prev = p;
                    if (!pointsTotalList.Contains(p))
                    {
                        pointsTotalList.Add(p);
                    }
                }

                if (negLen > 0 && i < pointsTotal - 2)
                {
                    var third = points[(i + 2) % pointsTotal].position;
                    addLen = lineLength - negLen;
                }
            }
        }

        private void PlaceFances()
        {
            var r = new System.Random(10000);
            var fences = new List<GameObject>();
            foreach (Transform fence in fenceHolder)
            {
                fences.Add(fence.gameObject);
            }

            while (fences.Count < pointsTotalList.Count)
            {
                GameObject instaFence = null;
                if (fences.Count % 2 == 0)
                {
                    instaFence = Instantiate(fence1Exampe[r.Next(fence1Exampe.Length)], fenceHolder);
                }
                else
                {
                    instaFence = Instantiate(fence2Exampe[r.Next(fence2Exampe.Length)], fenceHolder);
                }
                fences.Add(instaFence);
            }

            while (fences.Count > pointsTotalList.Count)
            {
                var fence = fences[fences.Count - 1];
                fences.Remove(fence);
                DestroyImmediate(fence);
            }

            r = new System.Random(10000);
            for (int i = 0; i < fences.Count; i++)
            {
                var fenceTrans = fences[i].transform;
                var inverce = r.Next(3);
                if (i < fences.Count - 1)
                {
                    Vector3 dir;
                    if (inverce > 1)
                    {
                        dir = (pointsTotalList[i + 1] - pointsTotalList[i]);
                    }
                    else
                    {
                        dir = (pointsTotalList[i] - pointsTotalList[i + 1]);
                    }
                    fenceTrans.forward = dir;
                    var scale = Vector3.one * (dir.magnitude / .9f);
                    scale.y = (lineLength / .9f);
                    fenceTrans.localScale = scale;
                    fenceTrans.position = pointsTotalList[i] + ((pointsTotalList[i + 1] - pointsTotalList[i]) * .5f);
                }
                else
                {
                    var dir = (pointsTotalList[i] - pointsTotalList[i - 1]);
                    fenceTrans.forward = dir;
                    var scale = Vector3.one * (dir.magnitude / .9f);
                    scale.y = (lineLength / .9f);
                    fenceTrans.localScale = scale;
                    fenceTrans.position = pointsTotalList[i] + ((pointsTotalList[i] - pointsTotalList[i - 1]) * .5f);
                }
            }
        }

        private void PlaceCubes()
        {
            var r = new System.Random(10000);
            var sticks = CorrectObjectsCount(cube, fenceHolder);

            for (int i = 0; i < sticks.Count; i++)
            {
                var stick = sticks[i];
                stick.transform.position = pointsTotalList[i];
            }

            var links = CorrectObjectsCount(fence2Exampe[0], linksHolder);

            for (int i = 1; i < links.Count; i++)
            {
                var inverce = r.Next(3);
                Vector3 dir;
                Vector3 dirForward;
                if (inverce > 1)
                {
                    dir = pointsTotalList[i - 1] - pointsTotalList[i];
                    dirForward = dir;
                    dirForward.y = pointsTotalList[i - 1].y;
                }
                else
                {
                    dir = pointsTotalList[i] - pointsTotalList[i - 1];
                    dirForward = dir;
                    dirForward.y = pointsTotalList[i].y;
                }
                links[i].transform.forward = dir;
                var scaleMult = (1 + Mathf.Abs(Vector3.Angle(dir, dirForward) % 45) / 45) * .5f + .5f;
                var scale = Vector3.one * (dir.magnitude / .9f) * scaleMult;
                scale.y = (lineLength / .9f);
                links[i].transform.localScale = scale;
                links[i].transform.position = pointsTotalList[i - 1] + ((pointsTotalList[i] - pointsTotalList[i - 1]) * .5f);
            }
        }

        private List<GameObject> CorrectObjectsCount(GameObject example, Transform holder)
        {
            
            var sticks = new List<GameObject>();
            foreach (Transform fence in holder)
            {
                sticks.Add(fence.gameObject);
            }

            while (sticks.Count < pointsTotalList.Count)
            {
                var instaFence = Instantiate(example, holder);
                sticks.Add(instaFence);
            }

            while (sticks.Count > pointsTotalList.Count)
            {
                var fence = sticks[0];
                sticks.Remove(fence);
                DestroyImmediate(fence);
            }
            return sticks;
        }

        private void OnDrawGizmos()
        {
            if (place)
            {
                //place = false;
                PlaceSubPoints();
                //PlaceFances();
                PlaceCubes();
            }

            for (int i = 0; i < pointsTotalList.Count - 1; i++)
            {
                Gizmos.DrawWireSphere(pointsTotalList[i], .1f);
                Gizmos.DrawLine(pointsTotalList[i], pointsTotalList[i + 1]);
            }
        }
    }
}
