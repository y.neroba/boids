using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine;

namespace Lazy {
    public class TableCard : MonoBehaviour {
        
        private RectTransform rectTransform;
        private Coroutine rotateRutine;

        void Awake() {
            rectTransform = transform as RectTransform;
        }

        public void RotateCard(float finAngle) {
            CheckOnRutine();
            rotateRutine = StartCoroutine(rotator(finAngle));
        }

        private void CheckOnRutine() {
            if (rotateRutine != null) {
                StopCoroutine(rotateRutine);
            }
        }
    
        private IEnumerator rotator(float zAngle)
        {
            var startRotate = rectTransform.localRotation;
            var finRotate = Quaternion.Euler(0, 0, zAngle);
            
            var steps = 10f;
            for (int i = 0; i < steps; i++)
            {
                rectTransform.localRotation = Quaternion.Lerp(startRotate, finRotate, i / steps);
                yield return null;
            }
            rectTransform.localRotation = finRotate;
        }
    }
}