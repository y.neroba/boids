using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

namespace Lazy {
    [RequireComponent(typeof(Image))]
    [RequireComponent(typeof(HandCard))]
    public class DeckCard : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
        [SerializeField] private Sprite  frontImage;

        private bool draging = false;
        private RectTransform rectTrans;
        private Vector3 lastMousePos = new Vector3();

        private void Start() {
            rectTrans = transform as RectTransform;
            rectTrans.localRotation = Quaternion.Euler(0, 0, Random.Range(-5, 5));
            var handScript = GetComponent<HandCard>();
            handScript.enabled = false;
        }

        public void OnPointerDown(PointerEventData eventData) {
            draging = true;
            lastMousePos = Input.mousePosition;
        }

        public void OnPointerUp(PointerEventData eventData) {
            draging = false;
            StartCoroutine(animationRutine());
        }

        private void Update() {
            if (!draging) {
                return;
            }

            var dif = Input.mousePosition - lastMousePos;
            rectTrans.position += dif;
            lastMousePos = Input.mousePosition;
        }

        private IEnumerator animationRutine()
        {
            var bottomPos = new Vector3(Screen.width * .5f, -rectTrans.sizeDelta.y * 1.1f);
            var startPos = transform.position;
            var animSpeed = 15f; 
            for (int i = 0; i < animSpeed; i++) {
                transform.position = Vector3.Lerp(startPos, bottomPos, i / animSpeed);
                yield return null;
            }
            GetComponent<Image>().sprite = frontImage;
            var handScript = GetComponent<HandCard>();
            handScript.enabled = true;
            handScript.InitFromTable();
            enabled = false;
        }
    }
}