using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lazy {
    public class HandCardsController : MonoBehaviour {

        [SerializeField, Range(1f, 20.0f)] private float lerpValue   = 2f;
        [SerializeField, Range(.1f, 1.5f)] private float cardsOffest = 1f;
        [SerializeField] private List<HandCard> cards;

        private float handWidth = 0f;

        IEnumerator Start() {
            for (int i = 0; i < cards.Count; i++) {
                cards[i].Init(this);
            }
            cardsOffest = .05f;
            for (int i = 0; i < 120; i++) {
                yield return null;
                cardsOffest = Mathf.Lerp(cardsOffest, 1f, i / 120f);
            }
        }

        void Update() {
            handWidth = (transform as RectTransform).sizeDelta.x;
            var cardWidth = 0f;
            var startDecOffset = 0f;
            if (cards.Count > 0) {
                cardWidth = (cards[0].transform as RectTransform).sizeDelta.x;
                startDecOffset = cardWidth * .5f;
            }
            var onOneCardWidth = (handWidth - cardWidth * .5f) / cards.Count;
            for (int i = 0, c = 0; i < cards.Count; i++) {
                if (cards[i].IsHandling) {
                    continue;
                }

                var rectT = cards[i].transform as RectTransform;
                var xPos = c * onOneCardWidth * cardsOffest + startDecOffset - handWidth * .5f;
                rectT.anchoredPosition = Vector2.Lerp(rectT.anchoredPosition, new Vector2(xPos, 0), Time.deltaTime * lerpValue);
                c++;
            }
        }

        public void PickupCard(HandCard card) {
            //cards.Remove(card);
            card.transform.parent = transform.parent;
        }

        public void AddNewCard(HandCard card) {
            card.transform.parent = transform;
            if (!cards.Contains(card)) {
                cards.Add(card);
                card.transform.SetAsLastSibling();
            }
        }

        public void HoldonCard(HandCard card) {
            card.transform.parent = transform;
            if (!cards.Contains(card)) {
                cards.Add(card);
            }
            cards.Sort();
            var indexOfCard = cards.IndexOf(card);
            for (int i = indexOfCard; i < cards.Count; i++) {
                cards[i].transform.SetSiblingIndex(i);
            }
        }

        public void RemoveCard(HandCard card) {
            cards.Remove(card);
            card.transform.parent = transform.parent;
            card.transform.SetAsLastSibling();
        }
    }
}