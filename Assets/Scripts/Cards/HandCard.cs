using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

namespace Lazy {
    [RequireComponent(typeof(TableCard))]
    public class HandCard : MonoBehaviour,  IPointerDownHandler,    IPointerUpHandler, 
                                            IPointerEnterHandler,   IPointerExitHandler, System.IComparable {

        private TableCard tableScript;
        private RectTransform rectTrans;
        private HandCardsController controller;
        private Vector3 holdOffset = new Vector3();
        private bool onTable    = false;
        private bool isHandling = false;
        public bool IsHandling  => isHandling;
        
        private void Awake() {
            rectTrans = transform as RectTransform;
            tableScript = GetComponent<TableCard>();
        }

        public void InitFromTable() {
            controller = FindObjectOfType<HandCardsController>();
            controller.AddNewCard(this);
            tableScript.RotateCard(0);
        }

        public void Init(HandCardsController controller) {
            this.controller = controller;
        }

        public void OnPointerDown(PointerEventData eventData) {
            isHandling = true;
            controller.PickupCard(this);
            holdOffset = transform.position - Input.mousePosition;
            transform.SetAsLastSibling();
        }

        public void OnPointerUp(PointerEventData eventData) {
            isHandling = false;
            if (eventData.button == PointerEventData.InputButton.Right) {
                onTable = false;
                controller.HoldonCard(this);
                return;
            }

            if (rectTrans.anchoredPosition.y > (rectTrans.sizeDelta.y * .7f)) {
                onTable = true;
                controller.RemoveCard(this);
            }
            else {
                onTable = false;
                controller.HoldonCard(this);
            }
            if (onTable) {
                tableScript.RotateCard(Random.Range(-10f,10f));
            }
        }

        public void OnPointerExit(PointerEventData eventData) {
            if (!onTable) {
                tableScript.RotateCard(0);
            }
        }

        public void OnPointerEnter(PointerEventData eventData) {
            if (!onTable) {
                tableScript.RotateCard(10);
            }
        }

        private void Update() {
            if (isHandling) {
                transform.position = Input.mousePosition + holdOffset;
            }
        }

        public int CompareTo(object obj)
        {
            var mb = obj as MonoBehaviour;
            if (!mb) {
                return -1;
            }

            if (transform.position.x < mb.transform.position.x) {
                return -1;
            }
            else if (transform.position.x > mb.transform.position.x) {
                return 1;
            }
            else {
                return 0;
            }
        }
    }
}
