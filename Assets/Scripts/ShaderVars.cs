using UnityEngine;

namespace Lazy {

    public class ShaderVars : Singletone<ShaderVars> {
        [SerializeField] private bool updateTime = true;
        private const string timeName = "_CustomTime";
        private const string screenResolution = "_ScreenResolution";

        private void Start() {
            Shader.SetGlobalVector(screenResolution, new Vector4(Screen.width, Screen.height, 0,0));
        }

        private void Update() {
            if (updateTime) {
                UpdateTime();
            }
        }

        private void UpdateTime() {
            var timeVector = new Vector4(
                Time.time,
                Time.time * .5f,
                Time.time * .25f,
                Time.time * .1f
                );
            Shader.SetGlobalVector(timeName, timeVector);
        }
    }
}
