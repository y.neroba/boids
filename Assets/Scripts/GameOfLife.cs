using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameOfLife : MonoBehaviour
{
    // The dimensions of the grid texture.
    public int2 gridSize;

    // The grid texture.
    public Texture2D gridTexture;

    // The compute shader for the Game of Life.
    public ComputeShader gameOfLifeShader;

    void Start()
    {
        // Initialize the grid texture.

        // Set the seed for the random number generator.
        int seed = (int)DateTime.Now.Ticks;
        Random.InitState(seed);

        // Initialize the grid with random values.
        for (int y = 0; y < gridSize.y; y++)
        {
            for (int x = 0; x < gridSize.x; x++)
            {
                // Generate a random value between 0 and 1.
                float alive = Random.value;

                // If the value is greater than 0.5, set the cell as alive.
                // Otherwise, set the cell as dead.
                float4 cell = new float4(alive > 0.5 ? 1 : 0, 0, 0, 0);
                // Store the initial state of the cell in the grid texture.
                gridTexture.SetPixel(x, y, new Color(cell.x, cell.x, cell.x, 1));
            }
        }
        gridTexture.Apply();
    }

    void Update()
    {
        // Dispatch the compute shader to update the grid texture.
        int kernel = gameOfLifeShader.FindKernel("CSMain");
        gameOfLifeShader.SetTexture(kernel, "gridTexture", gridTexture);
        gameOfLifeShader.SetInt("gridSizeX", gridSize.x);
        gameOfLifeShader.SetInt("gridSizeY", gridSize.y);
        gameOfLifeShader.Dispatch(kernel, gridSize.x, gridSize.y, 1);
    }
}
