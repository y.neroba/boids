using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lazy
{
    public class InverseKinematicSystem : MonoBehaviour
    {
        [SerializeField] private bool lockOnPosition;
        [SerializeField] private Transform directionPoint;

        private Transform followPoint;
        private List<InverseKinematic> planks = new List<InverseKinematic>();
        public List<Transform> possiblePoints = new List<Transform>();

        private void Start()
        {
            CreateListOfPlanks();
        }

        private void Update()
        {
            UpdateSystem();
        }

        private void CreateListOfPlanks()
        {
            planks = new List<InverseKinematic>();
            for(int i = 0; i < transform.childCount; i++)
            {
                if(transform.GetChild(i).TryGetComponent<InverseKinematic>(out var component))
                {
                    planks.Add(component);
                }
            }

            for(int i = 0; i < planks.Count - 1; i++)
            {
                planks[i].linkBefore = planks[i + 1];
            }
        }

        int iter = 0;

        [ExecuteInEditMode, ContextMenu("Update")]
        private void UpdateSystem()
        {
            if(planks.Count <= 0) {
                CreateListOfPlanks();
                return;
            }

            if(iter++ % 10 == 0) {
                GetNearPoint();
            }

            if(directionPoint) {
                planks[0].transform.position = directionPoint.position;
            }
            
            planks[0].Follow(followPoint.position);
            
            for(int i = 1; i < planks.Count; i++) {
                planks[i].Follow(planks[i - 1].transform.position);
            }

            if(lockOnPosition) {
                planks[planks.Count - 1].transform.position = transform.position;
                for(int i = planks.Count - 2; i >= 0; i--) {
                    planks[i].transform.position = planks[i + 1].endPoint;
                }
            }
        }

        private void GetNearPoint()
        {
            followPoint = possiblePoints[0];
            var currentDistance = Vector3.Distance(transform.position, possiblePoints[0].position);
            for(int i = 0; i < possiblePoints.Count; i++)
            {
                var dist = Vector3.Distance(transform.position, possiblePoints[i].position);
                if(dist < currentDistance)
                {
                    currentDistance = dist;
                    followPoint = possiblePoints[i];
                }
            }
        }

        private void OnDrawGizmos()
        {
            if(directionPoint)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawCube(directionPoint.position, new Vector3(.05f, .05f, .05f));
            }
        }
    }
}
