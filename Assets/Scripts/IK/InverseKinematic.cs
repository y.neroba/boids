using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lazy
{
    public class InverseKinematic : MonoBehaviour
    {
        public InverseKinematic linkBefore;
        [SerializeField] private float length = .5f;
        [SerializeField] private Vector2 lockAngles = new Vector2(0,.5f);
        [SerializeField] private bool useLimits;
        private Vector3 pos => transform.position;
        //private float angleXY = 0;
        //private float angleXZ = 0;
        //private float angleYZ = 0;

        private Vector3 target;

        public Vector3 endPoint { 
            get  {
                var dir = target - pos;
                var normDir = dir.normalized * length;
                return new Vector3(pos.x + normDir.x, pos.y + normDir.y, pos.z + normDir.z);
                //var dx = length * Mathf.Cos(angleXZ) * Mathf.Sin(angleXY);
                //var dy = length * Mathf.Cos(angleXZ) * Mathf.Cos(angleXY);
                //var dz = length * Mathf.Sin(angleXZ);
                //return new Vector3(pos.x + dx, pos.y + dy, pos.z + dz);
            }
        }

        public void Follow(Vector3 target)
        {
            this.target = target;
            var dir = target - pos;
            //var nextAngle = Mathf.Atan2(dir.normalized.y, dir.normalized.x);
            //var nextAngle = Mathf.Atan2(dir.normalized.y, dir.normalized.x);
            //var nextAngle2 = Mathf.Atan2(dir.normalized.z, dir.normalized.x);
            //var nextAngle3 = Mathf.Atan2(dir.normalized.y, dir.normalized.z);

            //if(useLimits)
            //{
            //    if(nextAngle < lockAngles.x)
            //    {
            //        angleXY = lockAngles.x;
            //    }
            //    else if (nextAngle > lockAngles.y)
            //    {
            //        angleXY = lockAngles.y;
            //    }
            //    else
            //    {
            //        angleXY = nextAngle;
            //    }
            //}
            //else
            //{
            //    angleXY = nextAngle;
            //    angleXZ = nextAngle2;
            //}

            //angleXY = nextAngle;
            //angleXZ = nextAngle2;
            //angleYZ = nextAngle3;

            //if(linkBefore)
            //{
            //var sAngle = angle;
            //angle = nextAngle;
            //var dist = Vector3.Distance(endPoint, linkBefore.transform.position);
            //if(dist < lockAngle * length)
            //{
            //    angle = sAngle;
            //    Debug.DrawLine(endPoint, linkBefore.transform.position, Color.red);
            //}
            //else
            //{
            //    Debug.DrawLine(endPoint, linkBefore.transform.position);
            //}
            //}
            //else
            //{
            //angle = nextAngle;
            //}

            var nextPos = pos + (pos + dir - endPoint);
            transform.position = nextPos;
        }

        private void OnDrawGizmos() {
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(pos, .05f);
            Gizmos.DrawLine(pos, endPoint);
            Gizmos.DrawWireSphere(endPoint, .05f);

            //if(useLimits)
            //{
            //    Gizmos.color = Color.red;
            //    var dx = length * Mathf.Cos(lockAngles.x) * Mathf.Sin(lockAngles.x);
            //    var dy = length * Mathf.Sin(lockAngles.x);
            //    var dz = length * Mathf.Sin(lockAngles.x) * Mathf.Sin(lockAngles.x);
            //    var lim = new Vector3(pos.x + dx, pos.y + dy, pos.z + dz);
            //    Gizmos.DrawLine(pos, lim);

            //    dx = length * Mathf.Cos(lockAngles.y) * Mathf.Sin(lockAngles.y);
            //    dy = length * Mathf.Sin(lockAngles.y);
            //    dz = length * Mathf.Sin(lockAngles.y) * Mathf.Sin(lockAngles.y);
            //    lim = new Vector3(pos.x + dx, pos.y + dy, pos.z + dz);
            //    Gizmos.DrawLine(pos, lim);
            //}
        }
    }
}
