using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lazy
{
    public class TestIKController : MonoBehaviour
    {
        [SerializeField] private float idleAmplitude = .1f;
        [SerializeField] private float idleSpeed = 2f;

        private List<InverseKinematicSystem> systems = new List<InverseKinematicSystem>();
        private float startPosY;
        private List<Transform> groundPoints = new List<Transform>();

        private void Awake()
        {
            startPosY = transform.position.y;

            CreateGroundPoints();

            systems = new List<InverseKinematicSystem>();
            for(int i = 0; i < transform.childCount; i++)
            {
                if(transform.GetChild(i).TryGetComponent(out InverseKinematicSystem system))
                {
                    system.possiblePoints = groundPoints;
                    systems.Add(system);
                }
            }
        }

        private void CreateGroundPoints()
        {
            groundPoints = new List<Transform>();
            var step = Mathf.PI / 5;
            var j = 0;
            for(float i = 0; i < 6.28f; i += step)
            {
                var go = new GameObject("Point " + j++);
                var pos = new Vector3(Mathf.Cos(i), 0, Mathf.Sin(i)) * .66f;
                go.transform.position = pos;
                groundPoints.Add(go.transform);
            }
        }

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.Space))
            {
                Jump();
            }

            transform.position = new Vector3(transform.position.x, Mathf.Sin(Time.time * idleSpeed) * idleAmplitude + startPosY, transform.position.z);
        }

        private void Jump()
        {

        }

        private void OnDrawGizmos()
        {
            for(int i = 0; i < groundPoints.Count; i++)
            {
                Gizmos.color = Color.magenta;
                Gizmos.DrawWireSphere(groundPoints[i].position, .05f);
            }
        }
    }
}
