using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lazy.Hex
{
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class HexCell : MonoBehaviour
    {
        [SerializeField] private MeshFilter filter;
        [SerializeField] private MeshRenderer render;
        [SerializeField] private float radius;

        private Vector3 center;
        private Vector3[] points = new Vector3[0];

        public void GenerateCell(Vector3 center, float outRadius)
        {
            this.center = center;
            radius = outRadius;

            PlacePoints();
            GenerateMesh();
        }

        private void PlacePoints()
        {
            points = new Vector3[13];

            points[0] = center;
            var innerHexMult = .75f;
            for (int i = 1; i < 7; i++)
            {
                var angle = ((60f * i) - 15f) * Mathf.Deg2Rad;
                var cos = Mathf.Cos(angle) * radius * innerHexMult;
                var sin = Mathf.Sin(angle) * radius * innerHexMult;

                points[i] = new Vector3(cos - sin, 0, cos + sin);
            }


            for (int i = 7; i < 13; i++)
            {
                var angle = ((60f * i) - 15f) * Mathf.Deg2Rad;
                var cos = Mathf.Cos(angle) * radius;
                var sin = Mathf.Sin(angle) * radius;

                points[i] = new Vector3(cos - sin, 0, cos + sin);
            }
        }

        public void ExtrudeCenter()
        {
            for (int i = 0; i < 7; i++)
            {
                points[i] = new Vector3(points[i].x, points[i].y + .1f, points[i].z);
            }
            GenerateMesh();
        }

        private void GenerateMesh()
        {
            Mesh mesh = new Mesh();
            int[] tris = new int[54];

            //Inner Circle
            CreateSquad(ref tris, 0, 0, 2, 1, 3);
            CreateSquad(ref tris, 6, 0, 4, 3, 5);
            CreateSquad(ref tris, 12, 0, 6, 5, 1);

            //Outer Circle
            CreateSquad(ref tris, 18, 1, 8, 7, 2);
            CreateSquad(ref tris, 24, 2, 9, 8, 3);
            CreateSquad(ref tris, 30, 3, 10, 9, 4);
            CreateSquad(ref tris, 36, 4, 11, 10, 5);
            CreateSquad(ref tris, 42, 5, 12, 11, 6);
            CreateSquad(ref tris, 48, 6, 7, 12, 1);

            //var uv = new Vector2[7];
            //uv[0] = new Vector2(.5f, .5f);
            //uv[1] = new Vector2(0f, .3f);
            //uv[2] = new Vector2(.5f, 0f);
            //uv[3] = new Vector2(1f, .3f);
            //uv[4] = new Vector2(1f, .7f);
            //uv[5] = new Vector2(.5f, 1f);
            //uv[6] = new Vector2(0f, .7f);

            mesh.vertices = points;
            mesh.triangles = tris;
            //mesh.uv = uv;
            mesh.RecalculateNormals();
            filter.mesh = mesh;
        }

        private void CreateSquad(ref int[] tris, int startIndex, int vert1, int vert2, int vert3, int vert4)
        {
            CreateTri(ref tris, startIndex, vert1, vert2, vert3);
            CreateTri(ref tris, startIndex + 3, vert1, vert4, vert2);
        }

        private void CreateTri(ref int[] tris, int startIndex, int vert1, int vert2, int vert3)
        {
            tris[startIndex] = vert1;
            tris[startIndex + 1] = vert2;
            tris[startIndex + 2] = vert3;
        }

        private void OnValidate()
        {
            if (!render)
            {
                render = GetComponent<MeshRenderer>();
            }
            if (!filter)
            {
                filter = GetComponent<MeshFilter>();
            }
        }
    }
}
