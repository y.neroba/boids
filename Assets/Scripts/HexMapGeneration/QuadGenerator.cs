using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Lazy.Hex
{
    public class QuadGenerator : MonoBehaviour
    {
        [SerializeField] private Vector2Int dim;
        [SerializeField] private float size = 1;
        [SerializeField] private MeshFilter filter;
        [SerializeField] private bool drawGizmo;
        private Vector3[] points = new Vector3[0];

        private void Start()
        {
            GenerateQuad();
        }

        [ContextMenu("Generate")]
        private void GenerateQuad()
        {
            var totalLength = dim.x * dim.y;
            points = new Vector3[totalLength];
            var halfX = dim.x / 2;
            var halfY = dim.y / 2;
            for (int y = 0; y < dim.y; y++)
            {
                for (int x = 0; x < dim.x; x++)
                {
                    points[x + y * dim.y] = new Vector3(x - halfX, 0, y - halfY) * size;
                }
            }

            var tris = new int[totalLength * 6];
            var uvMod = new Vector2[totalLength];
            for (int i = 0, t = 0; i < points.Length - dim.y - 1; i++, t += 6)
            {
                var x = (i % dim.x) / (float)dim.x;
                uvMod[i] = new Vector2(x, i / dim.y / (float)dim.y);
                if (i > 0 && (((i + 1) % dim.x) == 0 || ((i + 2) % dim.x) == 0))
                {
                    continue;
                }
                var v1 = i;
                var v2 = i + 1;
                var v3 = i + dim.x;
                var v4 = i + dim.x + 1;
                CreateSquad(ref tris, t, v1, v4, v3, v2);
            }

            var meshData = new Mesh();
            meshData.vertices = points;
            meshData.uv2 = uvMod;
            meshData.triangles = tris;
            meshData.RecalculateNormals();
            filter.mesh = meshData;
        }

        private void CreateSquad(ref int[] tris, int startIndex, int vert1, int vert2, int vert3, int vert4)
        {
            CreateTri(ref tris, startIndex, vert1, vert2, vert3);
            CreateTri(ref tris, startIndex + 3, vert1, vert4, vert2);
        }

        private void CreateTri(ref int[] tris, int startIndex, int vert1, int vert2, int vert3)
        {
            tris[startIndex] = vert1;
            tris[startIndex + 1] = vert2;
            tris[startIndex + 2] = vert3;
        }

        private void OnDrawGizmos()
        {
            if (!drawGizmo) return;
            for (int i = 0; i < points.Length; i++)
            {
                Handles.Label(points[i], i.ToString());
            }
        }
    }
}