using UnityEditor;
using UnityEngine;

namespace Lazy.Hex
{
    public class HexCellsMap : MonoBehaviour
    {
        [SerializeField] private Vector2Int dim;
        [SerializeField] private float cellOuterRadius = 1;
        [SerializeField] private float yOffset = 1;
        [SerializeField] private float xOffset = 1;
        [SerializeField] private HexCell hexCell;

        private Vector3[,] hexes = new Vector3[0,0];

        private void Start()
        {
            GenerateMap();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                var x = Random.Range(0, hexes.GetLength(0) * hexes.GetLength(1));
                var cell = transform.GetChild(x).GetComponent<HexCell>();
                cell.ExtrudeCenter();
            }
        }

        private void GenerateMap()
        {
            PlaceHexes();
            GenerateHexes();
        }

        private void PlaceHexes()
        {
            var cellInnerRadius = cellOuterRadius * 0.866025404f;

            hexes = new Vector3[dim.x, dim.y];
            for (int i = 0; i < dim.y; i++)
            {
                for (int j = 0; j < dim.x; j++)
                {
                    var addX = 0f;
                    if ((i +1) % 2 == 0)
                    {
                        addX = cellInnerRadius * .5f;
                    }
                    var x = (cellInnerRadius * j + addX) * 2f * xOffset;
                    var y = cellInnerRadius * i * 2f * yOffset;
                    var pos = new Vector3(x, 0, y);
                    hexes[j, i] = pos;
                }
            }
        }

        private void GenerateHexes()
        {
            while (transform.childCount > 0)
            {
                DestroyImmediate(transform.GetChild(0).gameObject);
            }

            for (int i = 0; i < hexes.GetLength(1); i++)
            {
                for (int j = 0; j < hexes.GetLength(0); j++)
                {
                    var pos = hexes[j, i];
                    var hex = Instantiate(hexCell, transform);
                    hex.transform.localPosition = pos;
                    hex.GenerateCell(new Vector3(), cellOuterRadius);
                }
            }
        }

        private void OnDrawGizmos()
        {
            for (int i = 0; i < hexes.GetLength(1); i++)
            {
                for (int j = 0; j < hexes.GetLength(0); j++)
                {
                    var pos = hexes[j,i];
                    Handles.Label(pos, $"{i}:{j}");
                }
            }
        }

    }
}
