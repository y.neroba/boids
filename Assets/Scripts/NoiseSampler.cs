﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseSampler : MonoBehaviour {
    [SerializeField] private Renderer renderer;
    [SerializeField] private int xTextSize = 100;
    [SerializeField] private int yTextSize = 100;
    [SerializeField] private float yTextScale = .5f;

    [ContextMenu("Update Texture")]
    public void UpdateTexture() {
        var text = NoiseGenerator.GetNoiseTexture(xTextSize, yTextSize, yTextScale);
        renderer.sharedMaterial.mainTexture = text;
    }

    private void OnValidate() {
        UpdateTexture();
    }
}
