﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class NoiseGenerator {
    public static Texture2D GetNoiseTexture(int xScale, int yScale, float scaler) {
        var text = new Texture2D(xScale, yScale);
        var matrix = GetNoiseMap(xScale, yScale, scaler);
        for (int i = 0; i < xScale; i++) {
            for (int j = 0; j < yScale; j++) {
                var avrg = matrix[i,j];
                text.SetPixel(i, j, new Color(avrg, avrg, avrg));
            }
        }
        text.Apply();
        return text;
    }

    public static float[,] GetNoiseMap(int xScale, int yScale, float scaler, float offsetX = 0, float offsetY = 0) {
        var resMatrix = new float[xScale,yScale];
        for (int i = 0; i < xScale; i++) {
            for (int j = 0; j < yScale; j++) {
                var x = (float)i / xScale * scaler + offsetX;
                var y = (float)j / yScale * scaler + offsetY;
                var avrg = Mathf.PerlinNoise(x, y);
                resMatrix[i, j] = avrg;
            }
        }
        return resMatrix;
    }
}
