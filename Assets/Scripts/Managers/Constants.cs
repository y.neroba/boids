﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lazy {
    public class Constants : Singletone<Constants> {
        [SerializeField] private Vector3 maxBounds = new Vector3(50,50,50);
        [SerializeField] private Vector3 minBounds = new Vector3(50,50,50);
        [SerializeField] private float searchRadius = 6f;
        [SerializeField] private float minStayRadius = 4f;
        [SerializeField] private float boidsSpeed = 5f;
        [SerializeField] private Transform boidsTarget;
        [Space(15f)]
        [SerializeField] private int startBoids = 10;
        [SerializeField] private Boid boid;
        [SerializeField] private Transform boidHolder;

        public Transform BoidsTarget => boidsTarget;
        public float BoidsSpeed => boidsSpeed;
        public float MinStayRadius => minStayRadius;
        public float SearchRadius => searchRadius;
        public Vector3 MaxBounds => maxBounds + transform.position;
        public Vector3 MinBounds => minBounds + transform.position;

        private void Start() {
            for (int i = 0; i < startBoids; i++) {
                var pos = new Vector3(Random.Range(MinBounds.x, MaxBounds.x), Random.Range(MinBounds.y, MaxBounds.y), Random.Range(MinBounds.z, MaxBounds.z));
                Instantiate(boid, pos, Quaternion.identity, boidHolder);
            }
        }

        private void OnDrawGizmos() {
            Gizmos.color = Color.magenta;   
            var centr = (MinBounds + MaxBounds) / 2f;
            Gizmos.DrawWireCube(centr, MaxBounds - MinBounds);
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireCube(centr, MaxBounds - MinBounds + Vector3.one * 2f);
        }
    }
}