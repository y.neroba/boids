﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassSpawner : MonoBehaviour {

    [SerializeField] private Mesh grassMesh;
    [SerializeField] private Material grassMaterial;
    [SerializeField] private Vector2Int dimentions;
    [SerializeField] private float      offset = .1f;
    [SerializeField] private float      noiseValue = 100;

    private Matrix4x4[] matrix;

    private void Awake() {
        GenerateMatrix();
    }

    private void GenerateMatrix() {
        matrix = new Matrix4x4[dimentions.x * dimentions.y];
        for (int y = 0; y < dimentions.y; y++) {
            for (int x = 0; x < dimentions.x; x++) {
                var matrixElement = y * dimentions.x + x;
                matrix[matrixElement] = transform.localToWorldMatrix;
                var scale = Vector3.one * (Mathf.PerlinNoise(x * noiseValue, y * noiseValue) + .1f) * .9f;
                matrix[matrixElement].SetTRS(transform.position + new Vector3(x + x * offset, 0, y + y * offset), Quaternion.Euler(-90, 0, 0), scale);
            }
        }
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            GenerateMatrix();
        }
        DrawGrass();
    }


    private void DrawGrass() {
        if (matrix.Length < 1024) {
            Graphics.DrawMeshInstanced(grassMesh, 0, grassMaterial, matrix);
        }
        else {
            var cycles = matrix.Length / 1024;
            for (int i = 0; i < cycles; i++) {
                Graphics.DrawMeshInstanced(grassMesh, 0, grassMaterial, matrix);
            }
        }
    }
}
