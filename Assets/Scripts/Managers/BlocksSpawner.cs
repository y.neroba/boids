﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Collections;
using Unity.Mathematics;

public class BlocksSpawner : MonoBehaviour {
    [SerializeField] private Mesh       boxMesh;
    [SerializeField] private Material   boxMaterial;
    [Space]
    [SerializeField] private int        xSize = 5;
    [SerializeField] private int        zSize = 5;
    [Space]
    [Range(0, 1f)]
    [SerializeField] private float      minValue = .1f;
    [SerializeField] private int        seed = 2021;

    private Coroutine spawner;
    private System.Random rand;
    private EntityManager entityManager;

    private void Start() {
        entityManager = World.Active.EntityManager;
        Spawn();
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            Clear();
            Spawn();
        }
    }

    private void Clear() {
        CleanUpEntitys();
    }

    private void Spawn() {
        rand = new System.Random(seed);
        var matrix = NoiseGenerator.GetNoiseMap(xSize, zSize, 5f, rand.Next(9999), rand.Next(9999));
        var archytype = entityManager.CreateArchetype(
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(Scale),
                typeof(RenderMesh)
                //typeof(BlockEntity)
            );
        var entityArray = new NativeArray<Entity>(xSize * zSize, Allocator.Temp);
        entityManager.CreateEntity(archytype, entityArray);
        for (int i = 0; i < xSize; i++) {
            for (int j = 0; j < zSize; j++) {
                if (matrix[i,j] < minValue) {
                    continue;
                }
                var indx = (i * zSize) + j;
                var entity = entityArray[indx];
                entityManager.SetSharedComponentData(entity, new RenderMesh {
                    mesh = boxMesh,
                    material = boxMaterial,
                    layer = 8
                });

                entityManager.SetComponentData(entity, new Translation {
                    Value = new float3(transform.position.x + i, matrix[i,j] * .5f, transform.position.z + j)
                });

                entityManager.SetComponentData(entity, new Scale {
                    Value = matrix[i, j]
                });
            }
        }
        entityArray.Dispose();

        if (spawner != null) {
            StopCoroutine(spawner);
        }
        spawner = StartCoroutine(spawnRutine());
    }

    private IEnumerator spawnRutine() {
        var shortDelay = new WaitForSeconds(.1f);
        yield return shortDelay;
    }

    private void CleanUpEntitys() {
        entityManager = World.Active?.EntityManager;
        if (entityManager == null) {
            return;
        }

        var entitys = entityManager.GetAllEntities();
        for (int i = 0; i < entitys.Length; i++) {
            entityManager.DestroyEntity(entitys[i]);
        }
        entitys.Dispose();
    }

    private void OnDestroy() {
        CleanUpEntitys();
    }
}