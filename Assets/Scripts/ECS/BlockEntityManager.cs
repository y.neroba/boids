﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public class BlockEntityManager : ComponentSystem {
    protected override void OnUpdate() {
        Entities.ForEach((ref Translation translation) => {
            translation.Value.y = math.sin(Time.time + translation.Value.x * 90f + translation.Value.z * 90f) * .1f;
        });
    }
}
