﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Lazy.Utils {
    [CustomEditor(typeof(VignetteGeneator))]
    public class VignetteGeneator_Inspector : Editor {
        VignetteGeneator gui_target;

        public override void OnInspectorGUI() {
            gui_target = (VignetteGeneator)target;
            if (GUILayout.Button("Update vignette")) {
                gui_target.UpdateImage();
            }
            base.OnInspectorGUI();
        }
    }
}
