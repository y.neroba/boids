using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lazy
{
    public class MeshPoints : MonoBehaviour
    {
        [SerializeField] private Mesh mesh;
        [SerializeField, Range(0, 10000)] private int pointsCount = 1;
        [SerializeField] private float pointSize = .1f;
        [SerializeField] private float lerpValue = .1f;
        [SerializeField] private int randomSeed = 10000000;
        [SerializeField] private Color pointsColor;
        [SerializeField] private Color pointsColor2;
        [SerializeField] private Transform[] points;
        [Header("Fern")]
        [SerializeField] private float r1 = .1f;
        [SerializeField] private float r2 = .5f;
        [SerializeField] private float r3 = .9f;
        [Space]
        [SerializeField] private float a = .16f;
        [SerializeField] private float b = 0.85f;
        [SerializeField] private float c = 0.04f;
        [SerializeField] private float d = 0.20f;
        [SerializeField] private float e = 0.26f;
        [SerializeField] private float f = 0.23f;
        [SerializeField] private float g = 0.24f;
        [SerializeField] private float k = 0.22f;
        [SerializeField] private float l = 0.28f;
        [SerializeField] private float m = 0.44f;
        [SerializeField] private float h = 1.6f;

        [SerializeField] private bool farn;
        [SerializeField] private bool fractal;
        [SerializeField] private bool meshVerts;


        private void GetNextPoints(System.Random r, ref float x, ref float y)
        {
            float nextX, nextY;
            float rval = (float)r.NextDouble();
            if (rval < r1)
            {
                nextX = 0;
                nextY = a * y;
            }
            else if (rval < r2)
            {
                nextX = b * x + c * y;
                nextY = -c * x + b * y + h;
            }
            else if (rval < r3)
            {
                nextX = d * x - e * y;
                nextY = f * x + k * y + h;
            }
            else
            {
                nextX = -0.15f * x + l * y;
                nextY = e * x + g * y + m;
            }

            x = nextX;
            y = nextY;
        }

        private void OnDrawGizmos()
        {
            var r = new System.Random(randomSeed);
            if (farn)
            {
                var x = 0f;
                var y = 0f;
                for (int i = 0; i < pointsCount; i++)
                {
                    GetNextPoints(r, ref x, ref y);
                    var p = new Vector3(x, y, 0) * transform.localScale.x;
                    Gizmos.color = Color.Lerp(pointsColor, pointsColor2, p.y / h);
                    var pos = transform.position + p;
                    Gizmos.DrawSphere(pos, pointSize);
                }
            }

            if (!mesh)
            {
                return;
            }

            //verts = new Vector3[3];
            //verts[0] = transform.position + Vector3.right;
            //verts[1] = transform.position + Vector3.left;
            //verts[2] = transform.position + Vector3.up;

            if (fractal)
            {
                var verts = new Vector3[points.Length];
                if (points.Length > 0)
                {
                    for (int i = 0; i < points.Length; i++)
                    {
                        verts[i] = points[i].position;
                    }
                }
                var first = verts[0];
                for (int i = 0; i < pointsCount; i++)
                {
                    Gizmos.color = Color.Lerp(pointsColor, pointsColor2, (float)i / pointsCount);
                    var second = verts[r.Next(verts.Length)];
                    var p = Vector3.Lerp(first, second, lerpValue);
                    Gizmos.DrawSphere(p, pointSize);
                    first = p;
                }
            }

            if (meshVerts)
            {
                var verts = mesh.vertices;
                var pointsCountLocal = pointsCount / verts.Length;
                for (int i = 0; i < verts.Length; i++)
                {
                    for (float j = 0; j < pointsCountLocal; j++)
                    {
                        Gizmos.color = Color.Lerp(pointsColor, pointsColor2, j / pointsCountLocal);
                        var index = (i + j) % verts.Length;
                        var nextPoint = verts[(int)index];
                        var p = Vector3.Lerp(verts[i], nextPoint, j / pointsCountLocal);
                        Gizmos.DrawSphere(transform.position + p, pointSize);
                    }
                }
            }
        }
    }
}
