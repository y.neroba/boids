﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Lazy.Utils {
    [RequireComponent(typeof(Image))]
    public class VignetteGeneator : MonoBehaviour {
        [SerializeField] private Image  resImage;
        [SerializeField] private bool   useHalfScreen;
        [SerializeField] private float  power;

        private float PI => Mathf.PI;

        private void Start() {
            UpdateImage();
        }

        public void UpdateImage() {
            CheckOnImage();
            var sprite = Generate();
            resImage.sprite = sprite;
        }

        private void CheckOnImage() {
            if (!resImage) {
                resImage = GetComponent<Image>();
            }
        }

        public Sprite Generate() {
            var width   = (float)Screen.width;
            var height  = (float)Screen.height;
            if (useHalfScreen) {
                width   *= .5f;
                height  *= .5f;
            }
            var txt = new Texture2D((int)width, (int)height);
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    var sinValX = 1 - Mathf.Pow(Mathf.Sin((i / width) * PI), power);
                    var sinValY = 1 - Mathf.Pow(Mathf.Sin((j / height) * PI), power);
                    txt.SetPixel(i, j, new Color(0, 0, 0, sinValY * sinValX));
                }
            }
            txt.wrapMode = TextureWrapMode.Clamp;
            txt.filterMode = FilterMode.Bilinear;
            txt.Apply();
            var sprite = Sprite.Create(txt, new Rect(0f, 0f, (int)width, (int)height), new Vector2(.5f, .5f), 100f);
            return sprite;
        }
    }
}
