﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Lazy {
    public class Boid : MonoBehaviour {
        [SerializeField] private Vector3 moveVeocity;
        [SerializeField] private MeshRenderer render;
        public Vector3 MoveVeocity => moveVeocity;
        private Constants rules;

        private void Start() {
            rules = Constants.Get;
            var centr = (rules.MinBounds + rules.MaxBounds) / 2f;
            moveVeocity = (centr - transform.position).normalized;
            transform.forward = moveVeocity;
        }

        private void Update() {
            transform.forward = Vector3.Lerp(transform.forward, moveVeocity.normalized, Time.deltaTime * 5f);
            transform.position += transform.forward * Time.deltaTime * rules.BoidsSpeed;
            if (!CorrectingWay()) {
                CheckOnNeighborsDirection();
            }
        }

        private void CheckOnNeighborsDirection() {
            var nearObjs = Physics.OverlapSphere(transform.position, rules.SearchRadius);
            var nearList = new List<Boid>();
            var avrg = new Vector3();
            for (int i = 0; i < nearObjs.Length; i++) {
                if (nearObjs[i].TryGetComponent(out Boid b)) {
                    if (b == this) {
                        continue;
                    }
                    if (Vector3.Dot(moveVeocity, b.MoveVeocity) < -.5f) {
                        continue;
                    }
                    nearList.Add(b);
                    avrg += nearObjs[i].transform.position;
                }
            }

            if (nearList.Count > 0) {
                avrg /= nearList.Count;
                var dist = Vector3.Distance(avrg, transform.position);
                moveVeocity = Vector3.Lerp(moveVeocity, (avrg - transform.position).normalized, 1 - dist / (rules.SearchRadius * .5f));
                for (int i = 0; i < nearList.Count; i++) {
                    dist = Vector3.Distance(nearList[i].transform.position, transform.position);
                    if (dist <= rules.MinStayRadius) {
                        moveVeocity = Vector3.Lerp(moveVeocity, (transform.position - nearList[i].transform.position).normalized, 1 - dist / rules.SearchRadius);
                    }
                    else {
                        moveVeocity = Vector3.Lerp(moveVeocity, nearList[i].MoveVeocity.normalized, 1 - dist / rules.SearchRadius);
                    }
                }
            }
        }

        private void CheckOnBonds() {
            //check on x
            if (transform.position.x > rules.MaxBounds.x) {
                transform.position = new Vector3(transform.position.x - rules.MaxBounds.x, transform.position.y, transform.position.z);
            }
            else if (transform.position.x < rules.MinBounds.x) {
                transform.position = new Vector3(transform.position.x + rules.MaxBounds.x, transform.position.y, transform.position.z);
            }
            //check on y
            if (transform.position.y > rules.MaxBounds.y) {
                transform.position = new Vector3(transform.position.x, transform.position.y - rules.MaxBounds.y, transform.position.z);
            }
            else if (transform.position.y < rules.MinBounds.y) {
                transform.position = new Vector3(transform.position.x, transform.position.y + rules.MaxBounds.y, transform.position.z);
            }
            //check on z
            if (transform.position.z > rules.MaxBounds.z) {
                transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - rules.MaxBounds.z);
            }
            else if (transform.position.z < rules.MinBounds.z) {
                transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + rules.MaxBounds.z);
            }
        }

        /// <summary>
        /// return true if was out of bounds
        /// </summary>
        /// <returns></returns>
        private bool CorrectingWay() {
            var wasOut = false;
            //check on x
            if (transform.position.x > rules.MaxBounds.x) {
                var newVel = new Vector3(-1f, moveVeocity.y, moveVeocity.z);
                moveVeocity = Vector3.Lerp(moveVeocity, newVel, 10f * Time.deltaTime);
                wasOut = true;
            }
            else if (transform.position.x < rules.MinBounds.x) {
                var newVel = new Vector3(1f, moveVeocity.y, moveVeocity.z);
                moveVeocity = Vector3.Lerp(moveVeocity, newVel, 10f * Time.deltaTime);
                wasOut = true;
            }
            //check on y
            if (transform.position.y > rules.MaxBounds.y) {
                var newVel = new Vector3(moveVeocity.x, -1f, moveVeocity.z);
                moveVeocity = Vector3.Lerp(moveVeocity, newVel, 10f * Time.deltaTime);
                wasOut = true;
            }
            else if (transform.position.y < rules.MinBounds.y) {
                var newVel = new Vector3(moveVeocity.x, 1f, moveVeocity.z);
                moveVeocity = Vector3.Lerp(moveVeocity, newVel, 10f * Time.deltaTime);
                wasOut = true;
            }
            //check on z
            if (transform.position.z > rules.MaxBounds.z) {
                var newVel = new Vector3(moveVeocity.x, moveVeocity.y, -1f);
                moveVeocity = Vector3.Lerp(moveVeocity, newVel, 10f * Time.deltaTime);
                wasOut = true;
            }
            else if (transform.position.z < rules.MinBounds.z) {
                var newVel = new Vector3(moveVeocity.x, moveVeocity.y, 1f);
                moveVeocity = Vector3.Lerp(moveVeocity, newVel, 10f * Time.deltaTime);
                wasOut = true;
            }

            return wasOut;
        }

        private void OnDrawGizmosSelected() {
            //Gizmos.color = Color.gray * 1.5f;
            //if (rules) {
            //    Gizmos.DrawWireSphere(transform.position, rules.SearchRadius);
            //    Gizmos.color = new Color(1,.5f,.5f);
            //    Gizmos.DrawWireSphere(transform.position, rules.MinStayRadius);
            //}
            //else {
            //    Gizmos.DrawWireSphere(transform.position, 2f);
            //}
            //Gizmos.color = Color.black;
            //Gizmos.DrawLine(transform.position, transform.position + transform.forward * 3f);
            Gizmos.color = Color.green;
            Gizmos.DrawLine(transform.position, transform.position + moveVeocity);
        }
    }
}