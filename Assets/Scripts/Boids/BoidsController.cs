using UnityEngine;

namespace Lazy.Boids
{
    public class BoidsController : MonoBehaviour
    {
        [SerializeField] private float boidsLerp = 1;
        [SerializeField] private float boidsSpeed = .1f;
        [SerializeField] private float circleSize;
        [SerializeField] private float centerForce = .1f;
        [SerializeField] private float boidsCollision = 10f;
        [SerializeField] private float boidsNear = 1f;
        [SerializeField] private int boidsCount;
        [SerializeField] private bool updateAuto;
        [SerializeField] private GameObject boid;
        private int[] neighbors;
        private float[] distances;
        private GameObject[] boids;

        private void Start()
        {
            Setup();
        }

        private void Update()
        {
            if (updateAuto)
            {
                UpdateBechaviour();
            }
        }

        private void Setup()
        {
            boids = new GameObject[boidsCount];
            for (int i = 0; i < boidsCount; i++)
            {
                boids[i] = Instantiate(boid, transform);
                boids[i].name = "Boid " + (i + 1);
                var dir = Random.insideUnitCircle;
                var spawnPosition = new Vector3(dir.x, 0, dir.y);
                boids[i].transform.SetPositionAndRotation(transform.position + spawnPosition * circleSize,
                    Quaternion.Euler(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360)));
            }
        }

        private void UpdateBechaviour()
        {
            if (boids == null || boids.Length != boidsCount)
            {
                return;
            }
            var dt = Time.deltaTime;
            var resultDirection = new Vector3();
            neighbors = new int[boidsCount];
            distances = new float[boidsCount];
            for (int i = 0; i < boidsCount; i++)
            {
                var count = GetNeighbors(ref neighbors, ref distances, boids[i].transform, boidsCollision);
                if (count > 0)
                {
                    var sameDir = new Vector3();
                    var crowdCenter = new Vector3();
                    var difDir = new Vector3();
                    for (int j = 0; j < count; j++)
                    {
                        if (distances[j] > boidsNear)
                        {
                            sameDir += boids[neighbors[j]].transform.forward;
                        }
                        else if (distances[j] < boidsNear)
                        {
                            difDir += boids[i].transform.position - boids[neighbors[j]].transform.position;
                        }
                        crowdCenter += boids[neighbors[j]].transform.position - boids[i].transform.position;
                    }
                    Debug.DrawLine(boids[i].transform.position, boids[i].transform.position + sameDir.normalized * 4, Color.green);
                    Debug.DrawLine(boids[i].transform.position, boids[i].transform.position + difDir.normalized * 4, Color.red);
                    Debug.DrawLine(boids[i].transform.position, boids[i].transform.position + crowdCenter.normalized * 4, Color.blue);
                    resultDirection = sameDir.normalized;
                    resultDirection += crowdCenter.normalized;
                    if (difDir.magnitude > 1)
                    {
                        resultDirection = difDir.normalized;
                    }
                    else
                    {
                        resultDirection += difDir.normalized;
                    }
                }

                var dir = transform.position - boids[i].transform.position;
                var koef = (dir.magnitude / circleSize);
                resultDirection += dir.normalized * koef;
                Debug.DrawLine(boids[i].transform.position, boids[i].transform.position + dir.normalized * koef * 4, Color.black);
                Debug.DrawLine(boids[i].transform.position, boids[i].transform.position + resultDirection * 4, Color.white);
                boids[i].transform.forward = Vector3.Lerp(boids[i].transform.forward, resultDirection.normalized, boidsLerp * boidsSpeed * .1f * dt);
                boids[i].transform.position += boids[i].transform.forward * (boidsSpeed * dt);
            }
        }

        private int GetNeighbors(ref int[] neighbors, ref float[] distances, Transform boid, float maxDistance)
        {
            var count = 0;
            for (int i = 0; i < boidsCount; i++)
            {
                if (boids[i] == boid)
                {
                    continue;
                }

                var distance = Vector3.Distance(boid.position, boids[i].transform.position);
                if (distance < maxDistance)
                {
                    distances[count] = distance;
                    neighbors[count] = i;
                    count++;
                }
            }

            return count;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = new Color(1, 1, 0);
            Gizmos.DrawWireSphere(transform.position, circleSize);
        }
    }
}
