using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using System;
using UnityEngine.UI;

namespace Lazy.WFC
{
    public class WFCManager : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField] private int gridX;
        [SerializeField] private int gridY;
        [SerializeField] private int cellSizeX;
        [SerializeField] private int cellSizeY;
        [SerializeField] private bool adaptiveEmpty;
        [SerializeField] private bool emptyBorder;
        [SerializeField, Range(0, 10)] private int emptyCellsMult = 1;
        [SerializeField, Range(0, 10)] private int boundForNoEmpty = 1;
        [SerializeField] private float animationDelay;
        [SerializeField] private bool randomizeEveryTime;
        [SerializeField] private int randomSeed = 123456789;

        [Header("Objects")]
        [SerializeField] private WFCUnit[] units;
        [SerializeField] private Image resultImage;

        private Texture2D[] allTextures;
        private WFCUnitData[,] grid = new WFCUnitData[0, 0];
        private System.Random rand;

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                var text = CreateAndGetTexture();
                var sprite = Sprite.Create(text, new Rect(0, 0, gridX * cellSizeX, gridY * cellSizeY), new Vector2(.5f, .5f));
                resultImage.sprite = sprite;
            }
        }

        public Texture2D CreateAndGetTexture()
        {
            CleanGrid();
            PlaceUnits();
            return GetTexture();
        }

        public Texture2D GetTexture()
        {
            Texture2D tex = new Texture2D(gridX * cellSizeX, gridY * cellSizeY);
            tex.filterMode = FilterMode.Point;
            for (int y = 0; y < gridY; y++)
            {
                for (int x = 0; x < gridX; x++)
                {
                    var gridTex = allTextures[grid[x, gridY - (y + 1)].TextureId];
                    for (int i = 0; i < cellSizeY; i++)
                    {
                        for (int j = 0; j < cellSizeX; j++)
                        {
                            tex.SetPixel(x * cellSizeX + j, y * cellSizeY + i, gridTex.GetPixel(j,i));
                        }
                    }
                }
            }
            tex.Apply();
            return tex;
        }

        private void CleanGrid()
        {
            allTextures = new Texture2D[units.Length];
            for (int i = 0; i < units.Length; i++)
            {
                allTextures[i] = units[i].texture;
            }
            grid = new WFCUnitData[gridX, gridY];
            if (randomizeEveryTime)
            {
                randomSeed = UnityEngine.Random.Range(0, int.MaxValue / 7);
            }
            rand = new System.Random(randomSeed);
        }


        private IEnumerator PlaceUnitsAnimation()
        {
            var delay = new WaitForSeconds(animationDelay);

            if (emptyBorder)
            {
                CreateEmptyBorders();
            }

            for (int y = 0; y < gridY; y++)
            {
                for (int x = 0; x < gridX; x++)
                {
                    PlaceOneCell(x, y);
                    yield return delay;
                }
            }
        }

        private IEnumerator UseNormalWFC()
        {
            if (emptyBorder)
            {
                CreateEmptyBorders();
            }
            yield return new WaitForSeconds(animationDelay);
            
            var randX = rand.Next(gridX - 2) + 1;
            var randY = rand.Next(gridY - 2) + 1;
            var randU = rand.Next(units.Length);
            CrasteAndPutToGrid(randX, randY, units[randU]);
            
            yield return new WaitForSeconds(animationDelay);

            for (int y = 0; y < gridY; y++)
            {
                for (int x = 0; x < gridX; x++)
                {
                    var candidates = new List<PossibleUnitData>();
                    for (int i = 0; i < gridY; i++)
                    {
                        for (int j = 0; j < gridX; j++)
                        {
                            if (grid[j, i] != null)
                            {
                            	continue;
                            }

                            var unit = PlaceOneCell(j, i, false);
                            if (unit == null)
                            {
                                continue;
                            }

                            if (candidates.Count <= 0)
                            {
                                candidates.Add(unit);
                            }
                            else if (candidates[0].units.Count > unit.units.Count)
                            {
                                candidates.Clear();
                                candidates.Add(unit);
                            }
                            else if (candidates[0].units.Count < unit.units.Count)
                            {
                                continue;
                            }
                            else
                            {
                                candidates.Add(unit);
                            }
                        }
                    }

                    if (candidates.Count == 0)
                    {
                        var hasZero = false;
                        for (int i = 0; i < gridY; i++)
                        {
                            for (int j = 0; j < gridX; j++)
                            {
                                if (grid[j, i] == null)
                                {
                                    hasZero = true;
                                    break;
                                }
                            }
                            if (hasZero)
                            {
                                break;
                            }
                        }
                        if (hasZero)
                        {
                            CleanGrid();
                            Debug.Log("CANT FIND!!!");
                            yield return UseNormalWFC();
                        }
                        else
                        {
                            yield break;
                        }
                    }
                    else
                    {
                        var random = rand.Next(candidates.Count);
                        var randomUnit = rand.Next(candidates[random].units.Count);
                        CrasteAndPutToGrid(candidates[random].x, candidates[random].y, candidates[random].units[randomUnit]);
                    }

                    yield return new WaitForSeconds(animationDelay);
                }
            }
        }

        private void PlaceUnits()
        {
            if (emptyBorder)
            {
                CreateEmptyBorders();
            }

            for (int y = 0; y < gridY; y++)
            {
                for (int x = 0; x < gridX; x++)
                {
                    PlaceOneCell(x, y);
                }
            }
        }

        private void CreateEmptyBorders()
        {
            WFCUnit empty = null;
            for (int i = 0; i < units.Length; i++)
            {
                if (units[i].ID == UnitTypes.Blank)
                {
                    empty = units[i];
                    break;
                }
            }

            for (int y = 0; y < gridY; y++)
            {
                for (int x = 0; x < gridX; x++)
                {
                    if (y == 0 || x == 0 || y == gridY - 1 || x == gridX - 1)
                    {
                        CrasteAndPutToGrid(x, y, empty);
                    }
                }
            }
        }

        private PossibleUnitData PlaceOneCell(int x, int y, bool createAfterDefine = true)
        {
            var possible = new List<WFCUnit>(units.Length);
            for (int i = 0; i < units.Length; i++)
            {
                possible.Add(units[i]);
            }

            if (grid[x, y] != null)
            {
                return null;
            }

            if (y > 0)
            {
                possible = CheckOnCell(x, y - 1, UnitSides.Down, possible);
            }
            if (x > 0)
            {
                possible = CheckOnCell(x - 1, y, UnitSides.Right, possible);
            }
            if (x < gridX - 1)
            {
                possible = CheckOnCell(x + 1, y, UnitSides.Left, possible);
            }
            if (y < gridY - 1)
            {
                possible = CheckOnCell(x, y + 1, UnitSides.Up, possible);
            }

            var mul = (float)emptyCellsMult;
            if (adaptiveEmpty)
            {
                mul = -Mathf.Sin(x * Mathf.PI * (1 / gridX)) * emptyCellsMult + (emptyCellsMult - 1);
                possible = ApplyEmptyCellsMult(possible, mul);
            }
            else if (emptyCellsMult != 1)
            {
                possible = ApplyEmptyCellsMult(possible, mul);
            }

            if (possible.Count <= 0)
            {
                //Debug.Log("possible count is 0!!! Restart...");
                //randomSeed = Random.Range(0, 10000);
                //CleanGrid();
                //PlaceUnits();
                return null;
            }

            if (createAfterDefine)
            {
                var random = rand.Next(possible.Count);
                CrasteAndPutToGrid(x, y, possible[random]);
                return null;
            }
            else
            {
                return new PossibleUnitData(x, y, possible);
            }
        }

        private void CrasteAndPutToGrid(int x, int y, WFCUnit cell)
        {
            //var unit = Instantiate(cell, objectsHolder.position + new Vector3(x * cellSizeX, -y * cellSizeY, 0), Quaternion.identity);
            //unit.transform.SetParent(objectsHolder);
            //unit.name = unit.name.Replace("Clone", $"{x}_{y}");
            var id = 0;
            for (int i = 0; i < units.Length; i++)
            {
                if (units[i].ID == cell.ID)
                {
                    id = i;
                    break;
                }
            }
            grid[x, y] = new WFCUnitData(id, cell.ID);
        }

        private List<WFCUnit> CheckOnCell(int x, int y, UnitSides side, List<WFCUnit> possible)
        {
            var nextUnit = grid[x, y];
            if (nextUnit != null)
            {
                RemoveUnsuitableUnits(possible, nextUnit, side);
            }
            return possible;
        }

        private List<WFCUnit> ApplyEmptyCellsMult(List<WFCUnit> possible, float mult)
        {
            var resList = new List<WFCUnit>();
            for (int i = 0; i < possible.Count; i++)
            {
                if (possible[i].ID == UnitTypes.Blank || possible[i].ID == UnitTypes.Dot)
                {
                    for (int j = 0; j < mult; j++)
                    {
                        resList.Add(possible[i]);
                    }
                }
                else
                {
                    resList.Add(possible[i]);
                }
            }
            return resList;
        }
        private Rule lastRule = new Rule(UnitSides.Down);
        private void RemoveUnsuitableUnits(List<WFCUnit> possible, WFCUnitData upperUnit, UnitSides side)
        {
            if (possible.Count == 0)
            {
                return;
            }

            var idUnit = units[0];
            for (int i = 0; i < units.Length; i++)
            {
                if (units[i].ID == upperUnit.ID)
                {
                    idUnit = units[i];
                    break;
                }
            }
            for (int i = 0; i < idUnit.Rules.Length; i++)
            {
                if (idUnit.Rules[i].Side == side)
                {
                    lastRule = idUnit.Rules[i];
                    break;
                }
            }

            if (lastRule.Variants.Length == 0)
            {
                return;
            }

            for (int i = 0; i < possible.Count; i++)
            {
                var found = false;

                for (int j = 0; j < lastRule.Variants.Length; j++)
                {
                    if (possible[i].ID == lastRule.Variants[j])
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    possible.RemoveAt(i);
                    i--;
                }
            }
        }
    }

    public class WFCUnitData
    {
        private int textureId;
        private UnitTypes id;
        public UnitTypes ID => id;
        public int TextureId => textureId;

        public WFCUnitData(int textureId, UnitTypes id)
        {
            this.textureId = textureId;
            this.id = id;
        }
    }

    public class PossibleUnitData
    {
        public PossibleUnitData(int x, int y, List<WFCUnit> units)
        {
            this.x = x;
            this.y = y;
            this.units = units;
        }

        public int x;
        public int y;
        public List<WFCUnit> units;
    }
}
