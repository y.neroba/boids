using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Lazy.WFC
{
    [RequireComponent(typeof(Image))]
    public class WFCUnit : MonoBehaviour
    {
        [SerializeField] private Image image;
        [SerializeField] private bool canRotate;

        [SerializeField] private UnitTypes id;
        [SerializeField] private Rule[] rules = new Rule[4]
        {
            new Rule(UnitSides.Up),
            new Rule(UnitSides.Right),
            new Rule(UnitSides.Down),
            new Rule(UnitSides.Left)
        };

		private void Awake()
		{
            if (canRotate)
			{
                transform.rotation = Quaternion.Euler(0, 0, 90 * Random.Range(0, 4));
			}
        }

		public Texture2D texture => image.sprite.texture;
		public UnitTypes ID => id;
        public Rule[] Rules => rules;

        private void OnValidate()
        {
            if (!image)
            {
                image = GetComponent<Image>();
            }
        }
    }

    [System.Serializable]
    public struct Rule
    {
        [SerializeField] private UnitSides side;
        [SerializeField] private UnitTypes[] variants;

        public UnitSides Side => side;
        public UnitTypes[] Variants => variants;

        public Rule(UnitSides side)
        {
            this.side = side;
            variants = new UnitTypes[0];
        }

		public override string ToString()
		{
            return side.ToString();
        }
    }
}
