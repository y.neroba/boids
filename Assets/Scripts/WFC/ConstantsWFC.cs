namespace Lazy.WFC
{
    public static class ConstantsWFC
    {
        
    }

    public enum UnitTypes
    {
        Blank = 0,
        Up = 1,
        Right = 2,
        Left = 3,
        Down = 4,
        Cross = 5,
        Dot = 6,
        UpDot = 7,
        RightDot = 8,
        LeftDot = 9,
        DownDot = 10,
        UpLeft = 11,
    }

    public enum UnitSides
    {
        Up = 0,
        Right = 1,
        Down = 2,
        Left = 3,
        Up_Left = 4,
        Up_Right = 5,
        Down_Left = 6,
        Down_Right = 7,
    }
}
