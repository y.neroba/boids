﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Lazy {
    [RequireComponent(typeof(TMP_Text))]
    [ExecuteInEditMode]
    public class TextProcessor : MonoBehaviour {
        private const string SIN_TAG        = "sin";
        private const string FLICKER_TAG    = "flicker";
        private const string SIZE_TAG       = "popup";
        private const string TREMBLING_TAG  = "trembling";
        
        [SerializeField] private bool onlyCharacters = false;
        [SerializeField, Min(0)] private int animDelay = 1;
        [Header("Sinus effect")]
        [SerializeField] private float power                = 1f;
        [SerializeField] private float sinSpeed             = 1f;
        [SerializeField, Min(1)] private float sinLength    = 1f;
        [Header("Flicker effect")]
        [SerializeField] private Color flickerStartColor    = Color.white;
        [SerializeField] private Color flickerEndColor      = Color.white;
        [SerializeField] private float flickerSpeed         = 1f;
        [Header("Flicker effect")]
        [SerializeField] private float sizeSpeed            = 1f;
        [SerializeField] private float sizePower            = 1f;
        [SerializeField] private float sizeOffset           = 1f;
        [Header("Trimbling effect")]
        [SerializeField] private bool   colorizeTrembling   = true;
        [SerializeField] private Color  tremblingColor       = Color.white;
        [SerializeField] private float  tremblingPower       = 1f;
        [SerializeField] private float  tremblingSmooth      = 1f;

        private bool updateLinks = true;
        private Coroutine animationRutine;
        private TMP_Text text;

        void Start() {
            text = GetComponent<TMP_Text>();
        }

        void Update() {
            if (updateLinks) {
                UpdateProcess();
            }

            if (Input.GetKeyDown(KeyCode.Space)) {
                ShowText();
            }
            if (Input.GetKeyDown(KeyCode.E)) {
                ExploseText();
            }
        }

        private void UpdateProcess() {
            if (!text) {
                return;
            }

            var textInfo = text.textInfo;
            var links = textInfo.linkInfo;
            var updateGeometry = false;
            var updateColors = false;
            if (links.Length > 0) {
                text.ForceMeshUpdate();
            }
            for (int i = 0; i < links.Length; i++) {
                switch (links[i].GetLinkID()) {
                    case SIN_TAG:
                        updateGeometry = true;
                        UpdateSinText(links[i].linkTextfirstCharacterIndex, links[i].linkTextLength);
                        break;
                    case FLICKER_TAG:
                        updateColors = true;
                        UpdateFlickerText(links[i].linkTextfirstCharacterIndex, links[i].linkTextLength);
                        break;
                    case SIZE_TAG:
                        updateGeometry = true;
                        UpdateSizeText(links[i].linkTextfirstCharacterIndex, links[i].linkTextLength);
                        break;
                    case TREMBLING_TAG:
                        updateGeometry = true;
                        updateColors = colorizeTrembling;
                        UpdateTremblingText(links[i].linkTextfirstCharacterIndex, links[i].linkTextLength);
                        break;
                }
            }
            if (links.Length > 0) {
                for (int i = 0; i < textInfo.meshInfo.Length; i++) {
                    var meshInfo = textInfo.meshInfo[i];
                    if (updateColors) {
                        meshInfo.mesh.colors32 = meshInfo.colors32;
                    }
                    if (updateGeometry) {
                        meshInfo.mesh.vertices = meshInfo.vertices;
                        text.UpdateGeometry(meshInfo.mesh, i);
                    }
                }
            }
        }

        private void UpdateSinText(int startChar, int len) {
            var textInfo = text.textInfo;
            for (int i = startChar; i < len + startChar; i++) {
                var charInfo = textInfo.characterInfo[i];
                if (!charInfo.isVisible) {
                    continue;
                }
                var verts = textInfo.meshInfo[charInfo.materialReferenceIndex].vertices;
                if (onlyCharacters) {
                    for (int j = 0; j < 4; j++) {
                        var orig = verts[charInfo.vertexIndex + j];
                        verts[charInfo.vertexIndex + j] = orig + new Vector3(0, Mathf.Sin((verts[charInfo.vertexIndex].x / sinLength) + Time.time * sinSpeed) * power, 0);
                    }
                }
                else {
                    for (int j = 0; j < 4; j++) {
                        var orig = verts[charInfo.vertexIndex + j];
                        verts[charInfo.vertexIndex + j] = orig + new Vector3(0, Mathf.Sin((orig.x / sinLength) + Time.time * sinSpeed) * power, 0);
                    }
                }
            }
        }

        private void UpdateSizeText(int startChar, int len) {
            var textInfo = text.textInfo;
            for (int i = startChar; i < len + startChar; i++) {
                var charInfo = textInfo.characterInfo[i];
                if (!charInfo.isVisible) {
                    continue;
                }
                var verts = textInfo.meshInfo[charInfo.materialReferenceIndex].vertices;
                var orig = verts[charInfo.vertexIndex];
                var sin = Mathf.Sin((Time.time / sizeSpeed) + sizeOffset) * sizePower;
                verts[charInfo.vertexIndex] = orig + new Vector3(sin, sin, 0);
                orig = verts[charInfo.vertexIndex + 1];
                verts[charInfo.vertexIndex + 1] = orig + new Vector3(sin, -sin, 0);
                orig = verts[charInfo.vertexIndex + 2];
                verts[charInfo.vertexIndex + 2] = orig + new Vector3(-sin, -sin, 0);
                orig = verts[charInfo.vertexIndex + 3];
                verts[charInfo.vertexIndex + 3] = orig + new Vector3(-sin, sin, 0);
            }
        }

        private void UpdateTremblingText(int startChar, int len) {
            var textInfo = text.textInfo;
            for (int i = startChar; i < len + startChar; i++) {
                var charInfo = textInfo.characterInfo[i];
                if (!charInfo.isVisible) {
                    continue;
                }
                var verts = textInfo.meshInfo[charInfo.materialReferenceIndex].vertices;
                for (int j = 0; j < 4; j++) {
                    var orig = verts[charInfo.vertexIndex + j];
                    var sTime = tremblingSmooth * Time.time;
                    var perlinX = (Mathf.PerlinNoise(Time.time * verts[charInfo.vertexIndex].x * tremblingSmooth, Time.time * tremblingSmooth) - .5f) * 2f * tremblingPower;
                    var perlinY = (Mathf.PerlinNoise(Time.time * tremblingSmooth, Time.time * verts[charInfo.vertexIndex].x * tremblingSmooth) - .5f) * 2f * tremblingPower;
                    verts[charInfo.vertexIndex + j] = orig + new Vector3(perlinX, perlinY, 0);
                }
            }
            if (colorizeTrembling) {
                ColorizeText(startChar, len, tremblingColor);
            }
        }

        private void UpdateFlickerText(int startChar, int len) {
            var color = Color.Lerp(flickerStartColor, flickerEndColor, (Mathf.Sin(Time.time * flickerSpeed) + 1) * .5f);
            ColorizeText(startChar, len, color);
        }

        private void ColorizeText(int startChar, int len, Color col) {
            var textInfo = text.textInfo;
            for (int i = startChar; i < len + startChar; i++) {
                var charInfo = textInfo.characterInfo[i];
                if (!charInfo.isVisible) {
                    continue;
                }
                var cols = textInfo.meshInfo[charInfo.materialReferenceIndex].colors32;
                for (int j = 0; j < 4; j++) {
                    cols[charInfo.vertexIndex + j] = col;
                }
            }
        }

        private void HideText(int startChar, int len) {
            var textInfo = text.textInfo;
            for (int i = startChar; i < len + startChar; i++) {
                var charInfo = textInfo.characterInfo[i];
                if (!charInfo.isVisible) {
                    continue;
                }
                var verts = textInfo.meshInfo[charInfo.materialReferenceIndex].vertices;
                for (int j = 0; j < 4; j++) {
                    verts[charInfo.vertexIndex + j] = new Vector3();
                }
            }
        }

        private void MoveText(int startChar, int len) {
            var textInfo = text.textInfo;
            for (int i = startChar; i < len + startChar; i++) {
                var charInfo = textInfo.characterInfo[i];
                if (!charInfo.isVisible) {
                    continue;
                }
                var verts = textInfo.meshInfo[charInfo.materialReferenceIndex].vertices;
                for (int j = 0; j < 4; j++) {
                    verts[charInfo.vertexIndex + j] = new Vector3();
                }
            }
        }

        public void ShowText() {
            if (animationRutine != null) {
                StopCoroutine(animationRutine);
            }
            animationRutine = StartCoroutine(showRutine());
        }

        public void ExploseText() {
            if (animationRutine != null) {
                StopCoroutine(animationRutine);
            }
            animationRutine = StartCoroutine(explosionRutine());
        }

        private IEnumerator showRutine() {
            updateLinks = false;
            var fullText = text.textInfo.characterCount;
            for (int i = 0; i < fullText; i++) {
                UpdateProcess();
                HideText(i, fullText - i);
                var textInfo = text.textInfo;
                for (int j = 0; j < textInfo.meshInfo.Length; j++) {
                    var meshInfo = textInfo.meshInfo[j];
                    meshInfo.mesh.vertices = meshInfo.vertices;
                    text.UpdateGeometry(meshInfo.mesh, j);
                }
                for (int j = 0; j < animDelay; j++) {
                    yield return null;
                }
            }
            updateLinks = true;
        }

        private IEnumerator explosionRutine() {
            updateLinks = false;
            var fullText = text.textInfo.characterCount;
            for (int i = 0; i < fullText; i++) {
                UpdateProcess();
                var textInfo = text.textInfo;
                for (int j = 0; j < textInfo.meshInfo.Length; j++) {
                    var meshInfo = textInfo.meshInfo[j];
                    meshInfo.mesh.colors32 = meshInfo.colors32;
                    meshInfo.mesh.vertices = meshInfo.vertices;
                    text.UpdateGeometry(meshInfo.mesh, j);
                }

                for (int j = 0; j < animDelay; j++) {
                    yield return null;
                }
            }
        }
    }
}
