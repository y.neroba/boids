using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MandelbrotDrawer : MonoBehaviour
{
    [SerializeField] private Color linesColor;
    [SerializeField] private int step = 2;
    [SerializeField] private float size = 10;
    [SerializeField, Range(0,5)] private float mult = 2;
    [SerializeField, Range(0,1000)] private int offset = 2;
    [SerializeField, Range(0,1000)] private int pointsCount = 10;
    private Vector3[] points = new Vector3[0];

    private void Setup()
    {
        points = new Vector3[pointsCount];

        for (int i = 0; i < pointsCount; i++)
        {
            var index = (float)i / pointsCount;
            //index = Mathf.Sin(index);
            //index = Mathf.Sqrt(index);

            var a = index * mult * Mathf.PI;
            var c = Mathf.Cos(a);
            var s = Mathf.Sin(a);

            points[i] = new Vector3(c, s) * size + transform.position;
        }
    }

    private void OnValidate()
    {
        Setup();
    }

    private void OnDrawGizmos()
    {
        if (points == null || points.Length != pointsCount)
        {
            return;
        }

        Gizmos.color = linesColor;
        for (int i = 0; i < points.Length; i++)
        {
            var item = points[i];
            var itemLine = points[i];
            var index = i + (int)(offset * i) + step;
            if (index >= points.Length)
            {
                index %= points.Length;
            }
            var itemNext = points[index];

            if (i == points.Length - 1)
            {
                itemLine = points[0];
            }
            else
            {
                itemLine = points[i + 1];
            }

            Gizmos.DrawLine(item, itemLine);
            Gizmos.DrawLine(item, itemNext);
        }
    }
}
