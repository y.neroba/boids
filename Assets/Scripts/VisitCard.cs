using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Lazy
{
    public class VisitCard : MonoBehaviour
    {
		[Header("Items")]
        [SerializeField] private TMP_Text text;
        [SerializeField] private Transform bacgrounds;
        [SerializeField] private Transform fronts;
		[Header("Settings")]
		[SerializeField] private int backImagesCount = 5;
		[SerializeField] private int frontImagesCount = 5;
		[SerializeField] private float backSizeMultiplyer = 1f;
		[SerializeField] private float frontSizeMultiplyer = 1f;
		[SerializeField] private float centerOffset = 1f;
		[SerializeField] private float sizeRandom = 1f;
		[SerializeField] private float rotateRange = 30;
		[SerializeField] private float moveSpeed = 1;
		[SerializeField] private float moveRadius = 1;
		[Header("Resources")]
		[SerializeField] private Sprite[] sprites;

		private List<Image> generatedFrontImages = new List<Image>();
		private List<Image> generatedBackImages = new List<Image>();

		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.Space))
			{
				Prepare();
				GenerateImages();
			}

			Animate();
		}

		private void Animate()
		{
			AnimateImages(generatedBackImages);
			AnimateImages(generatedFrontImages);
		}

		private void AnimateImages(List<Image> images)
		{
			for (int i = 0; i < images.Count; i++)
			{
				var trans = images[i].transform;
				var pos = new Vector3(
					Mathf.Cos((Time.time) * moveSpeed),
					Mathf.Sin((Time.time) * moveSpeed),
					  0) * moveRadius * Time.deltaTime;

				trans.position += pos;
			}
		}

		private void Prepare()
		{
			CleanImagesList(generatedBackImages);
			CleanImagesList(generatedFrontImages);
		}

		private void CleanImagesList(List<Image> list)
		{
			for (int i = 0; i < list.Count; i++)
			{
				Destroy(list[i].gameObject);
			}
			list.Clear();
		}

		private void GenerateImages()
		{
			generatedBackImages = SetupImagesList(bacgrounds, backImagesCount, backSizeMultiplyer, new Color(1, 1, 1, .7f));
			generatedFrontImages = SetupImagesList(fronts, frontImagesCount, frontSizeMultiplyer, new Color(1,1,1,1));
		}
		
		private List<Image> SetupImagesList(Transform pos, int count, float size, Color color)
		{
			var images = GenerateList(pos, count);
			FillImages(images, sprites, size);
			PlaceImages(images);
			ApplyColor(images, color);
			return images;
		}

		private List<Image> GenerateList(Transform pos, int count)
		{
			var generated = new List<Image>();
			for (int i = 0; i < count; i++)
			{
				var obj = new GameObject("image");
				obj.transform.SetParent(pos, false);
				generated.Add(obj.AddComponent<Image>());
			}
			return generated;
		}

		private void FillImages(List<Image> images, Sprite[] sprites, float size)
		{
			for (int i = 0; i < images.Count; i++)
			{
				images[i].sprite = sprites[Random.Range(0, sprites.Length)];
				images[i].SetNativeSize();
				images[i].rectTransform.sizeDelta *= (size + Random.Range(-sizeRandom, sizeRandom));
			}
		}

		private void PlaceImages(List<Image> cards)
		{
			for (int i = 0; i < cards.Count; i++)
			{
				var trans = cards[i].transform;
				trans.rotation = Quaternion.Euler(0, 0, Random.Range(-rotateRange, rotateRange));
				var offset = Random.insideUnitCircle * centerOffset;
				trans.position = trans.parent.position + (Vector3)offset;
			}
		}

		private void ApplyColor(List<Image> cards, Color color)
		{
			for (int i = 0; i < cards.Count; i++)
			{
				cards[i].color = color;
			}
		}
	}
}
