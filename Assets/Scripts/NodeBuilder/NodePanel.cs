using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Lazy.Nodes
{
    public class NodePanel : MonoBehaviour
    {
        [SerializeField] private TMP_Text nameField;


        public void ShowNode(Node node)
        {
            nameField.SetText(node.Name);
        }
    }
}