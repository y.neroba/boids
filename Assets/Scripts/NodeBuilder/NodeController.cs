using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lazy.Nodes {
    public class NodeController : Singletone<NodeController> {
        [SerializeField] private NodePanel panel;
        [SerializeField] private Node defaultNode;

        private Node currentNode;

        private void Awake() {
            defaultNode.Activate();
        }

        public void ActivateNode(Node node) {
            panel.ShowNode(node);

            if (currentNode != null){
                currentNode.Deactivate();
            }
            currentNode = node;
        }

        public void DeactivateNode(Node node) {
            if (currentNode != node){
                return;
            }
        }

        public void CreateNewNode(Node exampleNode)
        {
            if (!exampleNode)
            {

            }
        }
    }
}
