using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine;
using UnityEngine.UI;

namespace Lazy.Nodes {
    public class Node : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler {

        [SerializeField] private Image background;
        [SerializeField] private bool isDefaultNode;
        [SerializeField] private GameObject socet;
        [SerializeField] private NodePoint port;

        public string Name { get; private set; }

        private Color activeColor = new Color(.65f,.65f,.65f);
        private Color passiveColor = new Color(.4f,.4f,.4f);
        private bool isActive;
        private bool inHandler;

        private void Awake() {
            if (!isDefaultNode) {
                background.color = passiveColor;
            }
            else {
                socet.SetActive(false);
            }
        }

        private void Update() {
            if (inHandler)
            {
                var normalPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, transform.position.z);
                transform.position = normalPosition;
            }

            if (!isActive) {
                return;
            }

            if (Input.GetKeyDown(KeyCode.Delete)) {
                Deactivate();
                Delete();
            }
        }

        public void Delete() {
            if (isDefaultNode) {
                return;
            }
            Destroy(gameObject);
        }

        public void Deactivate()
        {
            isActive = false;
            background.color = passiveColor;
            NodeController.Get.DeactivateNode(this);
        }

        public void Activate() {
            isActive = true;
            background.color = activeColor;
            NodeController.Get.ActivateNode(this);

        }

        public void OnPointerClick(PointerEventData eventData) {
            if (isActive) {
                Deactivate();
            }
            else {
                Activate();
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (!isActive)
            {
                background.color = passiveColor;
            }

            inHandler = false;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            background.color = activeColor;

            inHandler = true;
        }
    }
}
