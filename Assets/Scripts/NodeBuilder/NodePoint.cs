using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

namespace Lazy.Nodes {
    public class NodePoint : MonoBehaviour, IPointerDownHandler {
        [SerializeField] private LineRenderer connector;
        private bool clicked;


        public void OnPointerDown(PointerEventData eventData)
        {
            clicked = true;
            connector.SetPosition(0, transform.position);
            connector.SetPosition(1, transform.position);
        }

        private void Update(){
            if (Input.GetMouseButtonUp(0)){
                clicked = false;
            }

            if (clicked){
                var pos = Input.mousePosition - transform.position;
                connector.SetPosition(1, Input.mousePosition);
                Debug.Log("Pos" + Input.mousePosition + " Pos2" + transform.position + " posRes" + pos);
            }
        }
    }
}
