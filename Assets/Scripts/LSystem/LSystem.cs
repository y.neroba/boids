﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lazy
{
    [ExecuteInEditMode]
    public class LSystem : MonoBehaviour
    {
        [SerializeField] private float maxLength = 10;
        [SerializeField, Range(0,360)] private float angleBtw = 1;
        [SerializeField, Range(0,360)] private float angle = 1;
        [SerializeField, Range(0,1)] private float angleRegression = 1;
        [SerializeField, Range(0,1)] private float regression = 1;

        [SerializeField] private int recursion = 10;

        private List<Vector3> allPoints;
        private List<Vector3> lastPoints;
        private float tmpLength;

        private void OnValidate()
        {
            allPoints = new List<Vector3>();
            lastPoints = new List<Vector3>();
            tmpLength = maxLength;
            GenerateCode();
        }

        private void GenerateCode()
        {
            allPoints.Add(transform.position);

            var ln = maxLength;
            var ang = angle;
            var c = Mathf.Cos(ang * Mathf.Deg2Rad);
            var s = Mathf.Sin(ang * Mathf.Deg2Rad);

            CreatePoints(ref ln, ref ang, c, s, 0);

            var index = 1;
            var count = 2;
            for (int i = 0; i < recursion; i++)
            {
                CreateNodes(ref ln, ref ang, out c, out s, index, count);
                index += count;
                count *= 2;
            }
        }

        private void CreateNodes(ref float ln, ref float ang, out float c, out float s, int startIndex, int count)
        {
            ModifyValues(ref ln, ref ang, out c, out s);
            for (int i = startIndex; i < startIndex + count; i++)
            {
                CreatePoints(ref ln, ref ang, c, s, i);
            }
        }

        private void CreatePoints(ref float ln, ref float ang, float c, float s, int index)
        {
            var ca = Mathf.Cos(angleBtw * Mathf.Deg2Rad);
            var sa = Mathf.Sin(angleBtw * Mathf.Deg2Rad);

            var dir = new Vector3(c - ca, s, 0) * ln;
            allPoints.Add(allPoints[index] + dir);

            dir = new Vector3(c + ca, s, 0) * ln;
            allPoints.Add(allPoints[index] + dir);
        }

        private void ModifyValues(ref float ln, ref float ang, out float c, out float s)
        {
            ln *= (1 - regression);
            ang = ang * (1 - angleRegression);

            c = Mathf.Cos(ang * Mathf.Deg2Rad);
            s = Mathf.Sin(ang * Mathf.Deg2Rad);
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.cyan;
            for (int i = 0; i < allPoints.Count; i++)
            {
                Gizmos.DrawSphere(allPoints[i], 1);
            }
        }
    }
}
