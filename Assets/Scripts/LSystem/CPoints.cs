﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CPoints : MonoBehaviour {
    [SerializeField] private Transform origin;
    [SerializeField] private int points = 50;
    [SerializeField] private int highLighted = 13;
    [SerializeField] private float distanceMul = 2f;
    [SerializeField] private float Power = 2f;
    [Range(0f,2f)]
    [SerializeField] private float turnFriction = .01f;
    private List<Transform> allPoints = new List<Transform>();

    private void Start() {
        CreatePoints();
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            PlacePoints();
        }
    }

    private void PlacePoints() {
        for (int i = 0; i < origin.childCount; i++) {
            var angle = 2 * Mathf.PI * turnFriction * i;
            var cosA = Mathf.Cos(angle);
            var sinA = Mathf.Sin(angle);
            var point = origin.GetChild(i);
            var pointPos = new Vector3 (cosA * sinA,  sinA * sinA, cosA) * Mathf.Pow((i / (float)origin.childCount), Power) * distanceMul;
            point.transform.position = origin.position + pointPos;
        }
    }


    [ContextMenu("Create")]
    private void CreatePoints() {
        CleanPoints();
        for (int i = 0; i < points; i++) {
            var point = new GameObject("Point" + i);
            point.transform.parent = origin;
            allPoints.Add(point.transform);
        }
        PlacePoints();
    }

    private void CleanPoints() {
        for (int i = 0; i < allPoints.Count; i++) {
            DestroyImmediate(allPoints[i].gameObject);
        }
        allPoints.Clear();
    }

    private void OnDrawGizmos() {
        for (int i = 0; i < allPoints.Count; i++) {
            if (allPoints[i] == null) {
                return;
            }

            if (i % highLighted == 0) {
                Gizmos.color = Color.green;
            }
            else {
                Gizmos.color = Color.white * .7f;
            }
            Gizmos.DrawSphere(allPoints[i].position, .1f);
            if (i + 1 < allPoints.Count) {
                Gizmos.color = Color.cyan;
                Gizmos.DrawLine(allPoints[i].position, allPoints[i + 1].position);
            }
        }
    }
}
