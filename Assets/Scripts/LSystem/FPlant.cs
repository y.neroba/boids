﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Lazy {
    public class FPlant : MonoBehaviour {
        private const char      X_RULE      = 'X';
        private const char      F_RULE      = 'F';
        private const char      R_T_RULE    = '+';
        private const char      L_T_RULE    = '-';
        private const char      REM         = '[';
        private const char      DEL         = ']';

        [SerializeField] private int    minLength = 5;
        [SerializeField] private int    maxLength = 10;
        [SerializeField] private int    liner = 5;
        [Space]
        [SerializeField] private float  radius  = 2f;
        [SerializeField] private string systemCode;
        [Space]
        [SerializeField] private Material meshMat;
        private Coroutine createRutine;
        private Stack<Transform> points;
        private void Start() {
            var meshRenderer = gameObject.AddComponent<MeshRenderer>();
            meshRenderer.sharedMaterial = meshMat;
            gameObject.AddComponent<MeshFilter>();

            GenerateCode();
            points = new Stack<Transform>();
            createRutine = StartCoroutine(createSystem());
        }

        private void GenerateCode() {
            var length = Random.Range(minLength, maxLength + 1);
            systemCode = string.Empty;
            systemCode += X_RULE;
            string newCode = string.Empty;
            for (int i = 0; i < length; i++) {
                for (int j = 0; j < systemCode.Length; j++) {
                    switch (systemCode[j]) {
                        case X_RULE:
                            newCode += "F[[X]-X]-F[-FX]+X";
                            break;
                        case F_RULE:
                            //newCode += "F+F";
                            break;
                    }
                }
                systemCode = newCode;
                newCode = string.Empty;
            }
        }

        private IEnumerator createSystem() {
            var basePoint   = new GameObject("FirstPoint");
            basePoint.transform.parent = transform;
            basePoint.transform.localPosition = new Vector3();
            var pointLength = 5f;
            var angle = 0;
            var lastPoint = basePoint.transform;
            for (int i = 0; i < systemCode.Length; i++) {
                switch (systemCode[i]) {
                    case F_RULE:
                        var p = new GameObject($"point {i + 1}");
                        p.transform.parent = transform;
                        var cosA = Mathf.Cos(Mathf.Deg2Rad * angle) * radius;
                        var sinA = Mathf.Sin(Mathf.Deg2Rad * angle) * radius;
                        p.transform.position = lastPoint.position + new Vector3(cosA - sinA, cosA + sinA, pointLength / 10f) * pointLength;
                        lastPoint = p.transform;
                        pointLength *= .99f;
                        yield return null;
                        GenerateAllMesh();
                        break;
                    case L_T_RULE:
                        angle -= 25;
                        break;
                    case R_T_RULE:
                        angle += 25;
                        break;
                    case REM:
                        points.Push(lastPoint);
                        break;
                    case DEL:
                        points.Pop();
                        break;
                }
            }

        }

        private void GenerateAllMesh() {    
            var totalChilds = transform.childCount;
            Mesh mesh = new Mesh();
            Vector3[] vertices = new Vector3[totalChilds * 4];
            int[] tris = new int[totalChilds * 6];
            
            for (int i = 0, k = 0, v = 0; k < totalChilds; i += 4, v += 3, k++) {
                if (k + liner + 1 >= totalChilds) {
                    break;
                }
                var l_b = transform.GetChild(k + liner).localPosition;
                var l_t = transform.GetChild(k).localPosition;
                var r_t = transform.GetChild(k + 1).localPosition;
                var r_b = transform.GetChild(k + liner + 1).localPosition;

                vertices[i]     = l_b;
                vertices[i + 1] = r_b;
                vertices[i + 2] = l_t;
                vertices[i + 3] = r_t;

                var tI = k * 6;
                tris[tI]        = i;
                tris[tI + 1]    = i + 2;
                tris[tI + 2]    = i + 1;
                tris[tI + 3]    = i + 2;
                tris[tI + 4]    = i + 3;
                tris[tI + 5]    = i + 1;
            }

            mesh.vertices = vertices;
            mesh.triangles = tris;
            mesh.RecalculateNormals();
            GetComponent<MeshFilter>().mesh = mesh;
        }

        private void OnDrawGizmos() {
            Gizmos.color = Color.cyan;
            for (int i = 0; i < transform.childCount; i++) {
                var child = transform.GetChild(i);
                //Gizmos.DrawSphere(child.position, .1f);
                if (i + 1 < transform.childCount) {
                    Gizmos.DrawLine(child.position, transform.GetChild(i + 1).position);
                }
                if (i + liner < transform.childCount) {
                    Gizmos.DrawLine(child.position, transform.GetChild(i + liner).position);
                }
            }
        }
    }
}