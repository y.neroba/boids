﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Lazy {
    public class ShapeSystem : MonoBehaviour {
        [SerializeField, Range(0f, 10f)] private float  turnFriction;
        [SerializeField] private int        pointsCount;
        [SerializeField] private float      radiusMul     = 1f;
        [SerializeField] private float      pow           = 1f;
        [SerializeField] private int        startOffest   = 0;
        [SerializeField] private GameObject example;
        [Space(15f)]
        [SerializeField, Range(0,1f)] private float  driverMove       = 0.0001f;
        [SerializeField] private float      driver         = 23;
        [SerializeField] private int        driverOffset     = 13;
        [SerializeField] private float      driverAngle      = 0f;

        private Coroutine createRutine;

        private void Start() {
            Application.targetFrameRate = 60;
            CreateSystem();
        }

        private void Update() {
            if (Input.GetKeyDown(KeyCode.Alpha1)) {
                StopAllCoroutines();
                createRutine = StartCoroutine(changeVisual());
            }
            if (Input.GetKeyDown(KeyCode.Alpha2)) {
                StopAllCoroutines();
                createRutine = StartCoroutine(animLoop());
            }
            if (Input.GetKeyDown(KeyCode.Alpha3)) {
                StopAllCoroutines();
                createRutine = StartCoroutine(sizeLoop());
            }
            if (Input.GetKeyDown(KeyCode.Alpha4)) {
                StopAllCoroutines();
                createRutine = StartCoroutine(rotator());
            }
            if (Input.GetKeyDown(KeyCode.Space)) {
                StopAllCoroutines();
            }
        }

        private void CreateSystem() {
            for (int i = 0; i < pointsCount; i++) {
                Instantiate(example, transform);
            }
            SetPointsOrder();
        }

        private void OnValidate() {
            SetPointsOrder();
        }

        private void SetPointsOrder() {
            for (int i = 0; i < transform.childCount; i++) {
                var point = transform.GetChild(i);
                var angle = 2 * Mathf.PI * i * turnFriction * .1f;
                var less = (angle + driverAngle) % (driver);
                angle += Mathf.Sin(less) * Mathf.Cos(less) * driverMove;
                var cosA = Mathf.Cos(angle);
                var sinA = Mathf.Sin(angle);
                var pointPos = new Vector3 (cosA * cosA, sinA * cosA, 0) * Mathf.Pow(i * radiusMul, pow);
                point.transform.position = pointPos + transform.position;
            }
        }

        private IEnumerator changeVisual() {
            turnFriction = 0f;
            while (turnFriction < .2f) {
                SetPointsOrder();
                yield return null;
                turnFriction += .00002f;
            }
        }

        private IEnumerator PointsAnimature() {
            driverMove  = 0f;
            driver      = 3;
            SetPointsOrder();
            while (driverMove < 0.0052f) {
                var mul = 1 - Mathf.Sin((driverMove / 0.0052f) * (Mathf.PI * .5f));
                driverMove += 0.00003f * mul + 0.000005f;
                SetPointsOrder();
                yield return null;
                Debug.Log($"{mul}");
            }
            driverMove = 0.0052f;
            SetPointsOrder();
            for (int i = 0; i < 10; i++) {
                yield return null;
            }
            while (driverMove > 0f) {
                var mul = 1 - Mathf.Sin((driverMove / 0.0052f) * (Mathf.PI * .5f));
                driverMove -= 0.00003f * mul + 0.00001f;
                SetPointsOrder();
                yield return null;
                Debug.Log($"{mul}");
            }
            driverMove = 0f;
            driver = 11;
            SetPointsOrder();
            while (driverMove < 0.009f) {
                var mul = 1 - Mathf.Sin((driverMove / 0.009f) * (Mathf.PI * .5f));
                driverMove += 0.00003f * mul + 0.000005f;
                SetPointsOrder();
                yield return null;
                Debug.Log($"{mul}");
            }
            driverMove = 0.009f;
            SetPointsOrder();
            for (int i = 0; i < 10; i++) {
                yield return null;
            }
            while (driverMove > 0f) {
                var mul = 1 - Mathf.Sin((driverMove / 0.009f) * (Mathf.PI * .5f));
                driverMove -= 0.00003f * mul + 0.00001f;
                SetPointsOrder();
                yield return null;
                Debug.Log($"{mul}");
            }
        }
        private IEnumerator animLoop() {
            while (true) {
                yield return StartCoroutine(PointsAnimature());
            }
        }

        private IEnumerator rotator() {
            while (true) {
                driverAngle += .005f;
                SetPointsOrder();
                yield return null;
            }
        }

        private IEnumerator PointsSizer() {
            driverMove = 0f;
            driver = 3;
            SetPointsOrder();
            for (int i = 0; i < 60; i++) {
                yield return null;
            }
            radiusMul = 1f;
            driverMove = 0.0052f;
            for (int i = 0; i < 240; i++) {
                radiusMul = Mathf.Cos(i / 240f * (Mathf.PI * .5f)) * .5f + .5f;
                //driverMove = 0.0052f;
                SetPointsOrder();
                yield return null;
            }
            driverMove = 0f;
            driver = 11;
            SetPointsOrder();
            for (int i = 0; i < 60; i++) {
                yield return null;
            }

        }

        private IEnumerator sizeLoop() {
            while (true) {
                yield return StartCoroutine(PointsSizer());
            }
        }

        //private void OnDrawGizmos() {
        //    for (int i = 0; i < transform.childCount; i++) {
        //        var child = transform.GetChild(i);
        //        if ((i + driverOffset) % driver == 0) {
        //            Gizmos.color = Color.cyan;
        //        }
        //        else {
        //            Gizmos.color = Color.white;
        //        }
        //        Gizmos.DrawSphere(child.position, 1f);
        //    }
        //}
    }
}