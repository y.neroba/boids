﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class RandomPlaneMeshGenerator : MonoBehaviour
{
    [SerializeField] private MeshFilter filtr;
    [SerializeField] private float radius = 1;
    [SerializeField] private int liner = 10;
    [SerializeField] private Material meshMat;
    [SerializeField] private Vector2 dim = new Vector2(5, 5);
    [SerializeField] private float randomOffset = .5f;
    [SerializeField] private float closer = 2;
    [SerializeField] private float offset = 15;
    [SerializeField] private float offset1 = 0.8f;
    [SerializeField] private float offset2 = 1;
    private List<Vector3> points = new List<Vector3>();
    private Dictionary<Vector3Int, List<Vector3>> hexes = new Dictionary<Vector3Int, List<Vector3>>();

    void Start()
    {
        if (!TryGetComponent<MeshRenderer>(out var meshRenderer))
        {
            meshRenderer = gameObject.AddComponent<MeshRenderer>();
            meshRenderer.sharedMaterial = meshMat;
        }
        if (!filtr)
        {
            filtr = gameObject.AddComponent<MeshFilter>();
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GenerateAllMesh();
        }
        else if (Input.GetKeyDown(KeyCode.C))
        {
            CreatePointsStruct();
            GenerateAllMesh();
        }
        else if (Input.GetKeyDown(KeyCode.R))
        {
            RandomizePoints();
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            MoveUpRandomHex();
        }
        else if (Input.GetKeyDown(KeyCode.N))
        {
            NormaliePoints();
        }
    }

    private void CreateHexesStruct()
    {
        hexes = new Dictionary<Vector3Int, List<Vector3>>();
        var startCenter = new Vector3(0, 0, 0);

    }

    private void CreatePointsStruct()
    {
        var startCenter = new Vector3(0, 0, 0);
        points = new List<Vector3>();

        points.AddRange(CreateOneHexPoints(startCenter));

        var startDist = radius * 2.4491f;

        var angle = 60f;
        //var offset = this.offset;
        var lim = 3;
        var dist = 0f;
        for (int j = 0; j < 3; j++)
        {
            lim *= 2;
            if (j > 1)
            {
                lim -= 6;
            }

            angle = 360f/ lim;
            dist = (startDist * (j + 1));

            for (int i = 0; i < lim; i++)
            {
                var localAngle = angle;
                var localDist = dist;
                if (j == 1)
                {
                    if ((i + 1) % 2 == 0)
                    {
                        localDist *= .866025f;
                    }
                }

                var value = Mathf.Deg2Rad * (localAngle * i - offset);
                var cosA = Mathf.Cos(value);
                var sinA = Mathf.Sin(value);
                var p = new Vector3(cosA - sinA, 0, cosA + sinA).normalized;
                var center = startCenter + p * localDist;
                points.AddRange(CreateOneHexPoints(center));
            }
        }

        //FOR HEXES PLANE
        //for (int k = 0; k < dim.y; k++)
        //{
        //    for (int j = 0; j < dim.x; j++)
        //    {
        //        cntr = cntr + new Vector3(dist, 0, 0);
        //        for (int i = 0; i < 6; i++)
        //        {
        //            cosA = Mathf.Cos(Mathf.Deg2Rad * (angle * i - 15)) * radius;
        //            sinA = Mathf.Sin(Mathf.Deg2Rad * (angle * i - 15)) * radius;
        //            var point = cntr + new Vector3(cosA - sinA, 0, cosA + sinA);
        //            points.Add(point);
        //        }
        //    }
        //    var verDist = dist * 0.866f;
        //    cntr = new Vector3((k % 2 == 0 ? -1 : 0) * dist * .5f, 0, cntr.z + verDist);
        //}
    }

    private void CreateOneHex(Vector3 cntr, Vector3Int coords)
    {
        var points = new List<Vector3>() {
            cntr
        };
        points.AddRange(CreateOneHexPoints(cntr));
        hexes[coords] = points;
    }

    private List<Vector3> CreateOneHexPoints(Vector3 cntr)
    {
        var hexPoints = new List<Vector3>();
        var angle = 60;
        float cosA, sinA;
        for (int i = 0; i < 6; i++)
        {
            cosA = Mathf.Cos(Mathf.Deg2Rad * (angle * i - 15)) * radius;
            sinA = Mathf.Sin(Mathf.Deg2Rad * (angle * i - 15)) * radius;
            var point = cntr + new Vector3(cosA - sinA, 0, cosA + sinA);
            hexPoints.Add(point);
        }
        return hexPoints;
    }

    private void GenerateAllMesh()
    {
        var totalChilds = points.Count;
        if (totalChilds == 0)
        {
            return;
        }
        Mesh mesh = new Mesh();
        Vector3[] vertices = new Vector3[totalChilds];
        int[] tris = new int[totalChilds * 2];
        for (int i = 0, k = 0, v = 0; k < (totalChilds - 1); i += 6, v += 12, k += 6)
        {
            if (k + liner + 1 >= totalChilds)
            {
                break;
            }
            var l_t = points[k + 0];
            var t_t = points[k + 1];
            var t_r = points[k + 2];
            var r_b = points[k + 3];
            var b_b = points[k + 4];
            var l_b = points[k + 5];

            vertices[i] = l_t;
            vertices[i + 1] = t_t;
            vertices[i + 2] = t_r;
            vertices[i + 3] = r_b;
            vertices[i + 4] = b_b;
            vertices[i + 5] = l_b;

            var tI = v;
            tris[tI] = i;
            tris[tI + 1] = i + 1;
            tris[tI + 2] = i + 2;

            tris[tI + 3] = i + 2;
            tris[tI + 4] = i + 3;
            tris[tI + 5] = i + 4;

            tris[tI + 6] = i + 4;
            tris[tI + 7] = i + 5;
            tris[tI + 8] = i;

            tris[tI + 9] = i;
            tris[tI + 10] = i + 2;
            tris[tI + 11] = i + 4;
        }
        var uv = new Vector2[vertices.Length];
        for (int i = 0; i < vertices.Length; i += 6)
        {
            uv[i] = new Vector2(0f, .3f);
            uv[i + 1] = new Vector2(.5f, 0f);
            uv[i + 2] = new Vector2(1f, .3f);
            uv[i + 3] = new Vector2(1f, .7f);
            uv[i + 4] = new Vector2(.5f, 1f);
            uv[i + 5] = new Vector2(0f, .7f);
        }
        mesh.vertices = vertices;
        mesh.triangles = tris;
        mesh.uv = uv;
        mesh.RecalculateNormals();
        filtr.mesh = mesh;
    }


    private void MoveUpRandomHex()
    {
        //var r = (int)Random.Range(0, dim.x * dim.y);
        var r = (int)Random.Range(0, points.Count) / 6;
        var firstIndex = r * 6;
        var lastIndex = firstIndex + 6;
        var pointsCenter = new Vector3();
        for (int i = firstIndex; i < lastIndex; i++)
        {
            pointsCenter += points[i];
        }
        pointsCenter /= 6;

        for (int i = firstIndex; i < lastIndex; i++)
        {
            var p = points[i];
            points[i] = p + Vector3.up * -.2f + (pointsCenter - p) * .05f;
        }
    }


    private void RandomizePoints()
    {
        var totalPoints = points.Count;
        if (totalPoints == 0)
        {
            return;
        }
        var randomOffsetMap = new Dictionary<Vector3, Vector3>();
        for (int i = 0; i < totalPoints; i++)
        {
            var point = points[i];
            var closePos = new Vector3((int)(point.x * closer), (int)(point.y * closer), (int)(point.z * closer));
            if (randomOffsetMap.ContainsKey(closePos))
            {
                continue;
            }
            //Random.Range(-randomOffset, randomOffset)
            var offset = point + new Vector3(Random.Range(-randomOffset, randomOffset), Random.Range(-randomOffset, randomOffset), 0);
            randomOffsetMap.Add(closePos, offset);
        }

        for (int i = 0; i < totalPoints; i++)
        {
            var point = points[i];
            var closePos = new Vector3((int)(point.x * closer), (int)(point.y * closer), (int)(point.z * closer));
            if (randomOffsetMap.ContainsKey(closePos))
            {
                points[i] = randomOffsetMap[closePos];
            }
        }
        GenerateAllMesh();
    }

    private void NormaliePoints()
    {
        var totalChilds = points.Count;
        if (totalChilds == 0)
        {
            return;
        }
        var randomOffsetMap = new Dictionary<Vector3, Vector3>();
        for (int i = 0; i < totalChilds; i++)
        {
            var point = points[i];
            var closePos = new Vector3((int)(point.x * closer), (int)(point.y * closer), (int)(point.z * closer));
            if (randomOffsetMap.ContainsKey(closePos))
            {
                continue;
            }
            var offset = point;
            randomOffsetMap.Add(closePos, offset);
        }

        for (int i = 0; i < totalChilds; i++)
        {
            var point = points[i];
            var closePos = new Vector3((int)(point.x * closer), 0, (int)(point.z * closer));
            if (randomOffsetMap.ContainsKey(closePos))
            {
                points[i] = randomOffsetMap[closePos];
            }
        }
        GenerateAllMesh();
    }

    private void OnValidate()
    {
        //CreatePointsStruct();
        //GenerateAllMesh();
    }
}
