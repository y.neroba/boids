﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoronoiDiagram : MonoBehaviour {
    [SerializeField] private Vector2 dim = new Vector2();
    private List<Vector3> points = new List<Vector3>();
    [SerializeField] private Color darkColor;
    [SerializeField] private Color lightColor;
    [SerializeField] private float grayScale = 20;
    [SerializeField] private float size = 20;


    void Start() {
        GenerateSimplePlane();
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            GenerateRandomPoints();
            var texture = GenerateNoiseTexture();
            GetComponentInChildren<MeshRenderer>().sharedMaterial.SetTexture("_MainTex", texture);
        }
    }

    private void GenerateSimplePlane() {
        if (transform.childCount > 0) {
            DestroyImmediate(transform.GetChild(0).gameObject);
        }

        var plane = new GameObject("Plane");
        plane.transform.parent = transform;
        plane.transform.localPosition = new Vector3();
        var meshRenderer = plane.AddComponent<MeshRenderer>();
        meshRenderer.sharedMaterial = new Material(Shader.Find("Standard"));
        var filtr       = plane.AddComponent<MeshFilter>();
        var mesh        = new Mesh();
        var vertices    = new Vector3[4];
        var uv          = new Vector2[4];
        var tris        = new int[6];

        vertices[0] = new Vector3(0, 0, 0);
        vertices[1] = Vector3.up * dim.y;
        vertices[2] = Vector3.up * dim.y + Vector3.right * dim.x;
        vertices[3] = Vector3.right * dim.x;

        tris[0] = 0;
        tris[1] = 1;
        tris[2] = 2;

        tris[3] = 0;
        tris[4] = 2;
        tris[5] = 3;

        uv[0] = new Vector2(0, 0);
        uv[1] = new Vector2(0, 1);
        uv[2] = new Vector2(1, 1);
        uv[3] = new Vector2(1, 0);
        
        mesh.vertices   = vertices;
        mesh.uv         = uv;
        mesh.triangles  = tris;
        mesh.RecalculateNormals();

        filtr.mesh = mesh;
        var texture = GenerateNoiseTexture();
        meshRenderer.sharedMaterial.SetTexture("_MainTex", texture);
        plane.transform.localScale = new Vector3(1, 1, 1);
    }

    private List<Vector3> GetNearPoints(int pointIndex, int x, int y) {
        var points = new List<Vector3>();
        points.Add(this.points[pointIndex]);
        var width = (int)dim.x;
        if (pointIndex > 0) { // left
            if (x % width == 0) {
                var point = new Vector3(
                    this.points[pointIndex + width - 1].x - width, 
                    this.points[pointIndex + width - 1].y, 0);
                points.Add(point);
            }
            else {
                points.Add(this.points[pointIndex - 1]);
            }
        }
        if (pointIndex < this.points.Count - 1) {
            if (x > 0 && x % (width - 1) == 0) {
                var point = new Vector3(
                    this.points[pointIndex - (width - 1)].x - width - 1,
                    this.points[pointIndex - (width - 1)].y, 0);
                points.Add(point);
            }
            else {
                points.Add(this.points[pointIndex + 1]);
            }
        }
        if (pointIndex >= width) {
            points.Add(this.points[pointIndex - width]);
            
            //if (x % width == 0) {
            //    var point = new Vector3(
            //        this.points[pointIndex - width + 1].x - width + 1,
            //        this.points[pointIndex - width + 1].y, 0);
            //    points.Add(point);
            //}
            //else {
                points.Add(this.points[pointIndex - width + 1]);
            //}

            if (pointIndex > width) {
                points.Add(this.points[pointIndex - width - 1]);
            }
        }
        if (pointIndex < this.points.Count - width) {
            points.Add(this.points[pointIndex + width]);
            points.Add(this.points[pointIndex + width - 1]);
            if (pointIndex < this.points.Count - 1 - width) {
                points.Add(this.points[pointIndex + width + 1]);
            }
        }
        return points;
    }

    private Texture2D GenerateNoiseTexture() {
        GenerateRandomPoints();
        var width = (int)dim.x * (int)size;
        var height = (int)dim.y * (int)size;
        var txt = new Texture2D(width, height);
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                var x = j / size;
                var y = i / size;
                var vec = new Vector3(x, y, 0);
                var pointIndex = (int)x + (int)y * (width / (int)size);
                var nPoints = GetNearPoints(pointIndex, (int)x, (int)y);
                var near = float.MaxValue;
                
                for (int k = 0; k < nPoints.Count; k++) {
                    var dist = Vector3.Distance(vec, nPoints[k]);
                    if (dist < near) {
                        near = dist;
                    }
                }

                var col = Color.Lerp(darkColor, lightColor, Mathf.Pow(1 - near, grayScale));
                txt.SetPixel(j, i, col);
            }
        }
        txt.Apply();
        return txt;
    }

    [ContextMenu("Generate Points")]
    private void GenerateRandomPoints() {
        points = new List<Vector3>();
        //var sphere  = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        //var sphereMesh = sphere.GetComponent<MeshFilter>().sharedMesh;
        for (int i = 0; i < dim.y; i++) {
            for (int j = 0; j < dim.x; j++) {
                //var randpoint = new Vector3(j, i, 0);
                var randpoint = new Vector3(j + Random.Range(0,.9f), i + Random.Range(0,.9f), 0);
                points.Add(randpoint);
            }
        }
    }

    private void OnDrawGizmos() {
        Gizmos.color = new Color(1, 0, 0, 1);
        for (int i = 0; i < points.Count; i++) {
            Gizmos.DrawWireSphere(transform.position + points[i], .1f);
        }
    }
}