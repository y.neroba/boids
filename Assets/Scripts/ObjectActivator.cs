﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectActivator : MonoBehaviour {
    private Coroutine activateRutine;

    private Vector3 startSize = new Vector3(1f,3f,1f);

    private void OnEnable() {
        var w = Random.Range(0, .3f) + .2f;
        var h = w + Random.Range(0, .5f);
        startSize = new Vector3(w, h, w);
        if (activateRutine != null) {
            StopCoroutine(activateRutine);
        }
        activateRutine = StartCoroutine(activate());
    }

    private IEnumerator activate() {
        float time = 0;
        float animateTime = .5f;
        transform.localScale = new Vector3(0f, 0f, 0f);

        while (time < animateTime) {
            time += Time.deltaTime;
            yield return null;
            transform.localScale = Vector3.Lerp(transform.localScale, startSize, time / animateTime);
            transform.position = new Vector3(transform.position.x, transform.localScale.y *.5f, transform.position.z);
        }
        transform.localScale = startSize;
    }
}
