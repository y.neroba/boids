﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lazy {
    public class CameraController : MonoBehaviour {
        private const string mouseWheel = "Mouse ScrollWheel";

        [SerializeField] private Transform  rotatePoint;
        [SerializeField] private Transform  cameraPoint;
        [SerializeField] private Camera     camera;
        [Space]
        [SerializeField] private float      rotateSpeed = 5f;
        [SerializeField] private float      moveSpeed   = 5f;
        [Space]
        [SerializeField] private float      minZoom   = 15f;
        [SerializeField] private float      maxZoom   = 35f;
        private Vector3 LPos;
        private Vector3 RPos;
        private void Update() {
            CheckOnMove();
            CheckOnRotation();
            CheckOnZoom();
        }

        private void CheckOnMove() {
            if (Input.GetMouseButtonDown(0)) {
                LPos = Input.mousePosition;
            }
            if (Input.GetMouseButton(0)) {
                var mult = Time.deltaTime * moveSpeed;
                LPos = Vector3.Lerp(LPos, Input.mousePosition, mult);
                var dif = LPos - Input.mousePosition;
                var convPos = new Vector3(dif.x, 0, dif.y) * .1f;
                
                var normDirForward = CamDirToFlat().normalized;
                var normDirRight = new Vector3(normDirForward.z, 0, -normDirForward.x);
                var finDir  = normDirForward * convPos.z + normDirRight * convPos.x;
                rotatePoint.position = Vector3.Lerp(rotatePoint.position, rotatePoint.position + finDir, mult);
            }
        }

        private void CheckOnRotation() {
            if (Input.GetMouseButtonDown(1)) {
                RPos = Input.mousePosition;
            }
            if (Input.GetMouseButton(1)) {
                var mult = Time.deltaTime * rotateSpeed;
                RPos = Vector3.Lerp(RPos, Input.mousePosition, mult);
                var dif = (Input.mousePosition - RPos) * .01f;
                var eulers = rotatePoint.rotation.eulerAngles;
                rotatePoint.rotation = Quaternion.Euler(eulers.x, eulers.y + dif.x, eulers.z);
            }
        }

        private void CheckOnZoom() {
            if (Input.GetAxis(mouseWheel) != 0) {
                camera.orthographicSize -= Input.GetAxis(mouseWheel) * 3f;
                camera.orthographicSize = Mathf.Clamp(camera.orthographicSize, minZoom, maxZoom);
            }
        }

        private Vector3 CamDirToFlat() {
            var dir = rotatePoint.position - cameraPoint.position;
            return new Vector3(dir.x, 0, dir.z);
        }

        private void OnDrawGizmos() {
            Gizmos.color = Color.magenta;
            Gizmos.DrawWireSphere(rotatePoint.position, .2f);
            Gizmos.DrawLine(rotatePoint.position, cameraPoint.position);
            Gizmos.DrawWireSphere(cameraPoint.position, .2f);
        }
    }
}