namespace Lazy
{
    public interface ICommandController
    {
        public void Add();
        public void Undo();
    }
}
